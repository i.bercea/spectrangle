package com.model;

public class PlayListUnit {
	private int tile_index = -1;
	private int board_index = -1;
	private String tile_encoding = null;
	public PlayListUnit(int tile_index, int board_index, String tile_encoding) {
		super();
		this.tile_index = tile_index;
		this.board_index = board_index;
		this.tile_encoding = tile_encoding;
	}
	public int getTile_index() {
		return tile_index;
	}
	public void setTile_index(int tile_index) {
		this.tile_index = tile_index;
	}
	public int getBoard_index() {
		return board_index;
	}
	public void setBoard_index(int board_index) {
		this.board_index = board_index;
	}
	public String getTile_encoding() {
		return tile_encoding;
	}
	public void setTile_encoding(String tile_encoding) {
		this.tile_encoding = tile_encoding;
	}
	
}
