package com.model;

public class Tile {
	
	private Character left = null;
	private Character right = null;
	private Character vertical = null;
	private int values = 0;
	private int aspect = -1;
	private int state = -1;
	
	public Tile(String input) {
		char[] encode = input.toCharArray();
		this.values = (int)encode[4] - (int)'0';
		this.state = (int)encode[0] - (int)'0';
		if(state==0) {
			this.left = encode[3];
			this.right = encode[1];
			this.vertical = encode[2];
			this.aspect = Aspect.upwards;
		}
		else if(state==1) {
			this.left = encode[2];
			this.right = encode[1];
			this.vertical = encode[3];
			this.aspect = Aspect.downwards;
		}
		else if(state==2) {
			this.left = encode[2];
			this.right = encode[3];
			this.vertical = encode[1];
			this.aspect = Aspect.upwards;
		}
		else if(state==3) {
			this.left = encode[1];
			this.right = encode[3];
			this.vertical = encode[2];
			this.aspect = Aspect.downwards;
		}
		else if(state==4) {
			this.left = encode[1];
			this.right = encode[2];
			this.vertical = encode[3];
			this.aspect = Aspect.upwards;
		}
		else if(state==5) {
			this.left = encode[3];
			this.right = encode[2];
			this.vertical = encode[1];
			this.aspect = Aspect.downwards;
		}
	}	
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		Tile sTile = (Tile)obj;
		if(
				(sTile.getLeft().equals(left)&&sTile.getRight().equals(right)&&sTile.getVertical().equals(vertical)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(left)&&sTile.getRight().equals(vertical)&&sTile.getVertical().equals(right)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(right)&&sTile.getRight().equals(left)&&sTile.getVertical().equals(vertical)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(right)&&sTile.getRight().equals(vertical)&&sTile.getVertical().equals(left)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(vertical)&&sTile.getRight().equals(right)&&sTile.getVertical().equals(left)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(vertical)&&sTile.getRight().equals(left)&&sTile.getVertical().equals(right)&&sTile.getValues()==values))
			return true;
		else
			return false;
	}
	
	public Character getLeft() {
		return left;
	}
	
	public void setLeft(Character left) {
		this.left = left;
	}
	
	public Character getRight() {
		return right;
	}
	
	public void setRight(Character right) {
		this.right = right;
	}
	
	public Character getVertical() {
		return vertical;
	}
	
	public void setVertical(Character vertical) {
		this.vertical = vertical;
	}
	
	public int getValues() {
		return values;
	}
	
	public void setValues(int values) {
		this.values = values;
	}
	
	public int getAspect() {
		return aspect;
	}

	private void setTile(String input) {
		char[] encode = input.toCharArray();
		this.values = (int)encode[4] - (int)'0';
		this.state = (int)encode[0] - (int)'0';
		if(state==0) {
			this.left = encode[3];
			this.right = encode[1];
			this.vertical = encode[2];
			this.aspect = Aspect.upwards;
		}
		else if(state==1) {
			this.left = encode[2];
			this.right = encode[1];
			this.vertical = encode[3];
			this.aspect = Aspect.downwards;
		}
		else if(state==2) {
			this.left = encode[2];
			this.right = encode[3];
			this.vertical = encode[1];
			this.aspect = Aspect.upwards;
		}
		else if(state==3) {
			this.left = encode[1];
			this.right = encode[3];
			this.vertical = encode[2];
			this.aspect = Aspect.downwards;
		}
		else if(state==4) {
			this.left = encode[1];
			this.right = encode[2];
			this.vertical = encode[3];
			this.aspect = Aspect.upwards;
		}
		else if(state==5) {
			this.left = encode[3];
			this.right = encode[2];
			this.vertical = encode[1];
			this.aspect = Aspect.downwards;
		}
	}
	
	public Tile setAspect(int[] place) {
		int r = place[0];
		int c = place[1];
		String temp = toEncoding();		
		if((r+c)%2==0) {
			this.aspect = Aspect.upwards;
			if(state==1||state==3||state==5) {
				state = (state+1)%6;
				setTile(state+temp.substring(1, temp.length()));
			}
		}
		else {
			this.aspect = Aspect.downwards;
			if(state==0||state==2||state==4) {
				state = (state+1)%6;
				setTile(state+temp.substring(1, temp.length()));
			}
		}
		return this;
	}
	
	public String toEncoding() {
		if(state==0)
			return String.format("%d%c%c%c%d", state, right, vertical, left, values);
		else if(state==1)
			return String.format("%d%c%c%c%d", state, right, left, vertical, values);
		else if(state==2)
			return String.format("%d%c%c%c%d", state, vertical, left, right, values);
		else if(state==3)
			return String.format("%d%c%c%c%d", state, left, vertical, right, values);
		else if(state==4)
			return String.format("%d%c%c%c%d", state, left, right, vertical, values);
		else if(state==5)
			return String.format("%d%c%c%c%d", state, vertical, right, left, values);
		return null;
	}
	
	public Tile turn(int angle) {
		if(angle == Angle.turn_0)
			return this;
		else if(angle == Angle.turn_120) {
			if(state==0)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, right, vertical, left, values));
			else if(state==1)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, right, left, vertical, values));
			else if(state==2)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, vertical, left, right, values));
			else if(state==3)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, left, vertical, right, values));
			else if(state==4)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, left, right, vertical, values));
			else if(state==5)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, vertical, right, left, values));
		}
		else if(angle == Angle.turn_240){
			if(state==0)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, right, vertical, left, values));
			else if(state==1)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, right, left, vertical, values));
			else if(state==2)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, vertical, left, right, values));
			else if(state==3)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, left, vertical, right, values));
			else if(state==4)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, left, right, vertical, values));
			else if(state==5)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, vertical, right, left, values));
		}
		return null;
	}
	
}
