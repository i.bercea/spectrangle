package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import com.project.*;

class TileTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() {
		Tile tile = new Tile("0ABC2");
		Tile copy = new Tile("0ABC2");
		assertTrue(tile.getAspect() == Aspect.upwards);
		assertTrue(tile.getLeft() == 'C');
		assertTrue(tile.getRight() == 'A');
		assertTrue(tile.getVertical() == 'B');
		assertTrue(tile.getValues() == 2);
		assertTrue(tile.equals(copy));
		tile.setLeft('D');
		assertTrue(tile.getLeft() == 'D');
		tile.setRight('E');
		assertTrue(tile.getRight() == 'E');
		tile.setVertical('F');
		assertTrue(tile.getVertical() == 'F');
		tile.setValues(5);
		assertTrue(tile.getValues() == 5);
		assertEquals(tile.toEncoding(), "0EFD5");
		tile = tile.turn(Angle.turn_120);
		assertTrue(tile.getLeft() == 'F');
		assertTrue(tile.getRight() == 'D');
		assertTrue(tile.getVertical() == 'E');
		tile = tile.turn(Angle.turn_120);
		assertTrue(tile.getLeft() == 'E');
		assertTrue(tile.getRight() == 'F');
		assertTrue(tile.getVertical() == 'D');
	}

}
