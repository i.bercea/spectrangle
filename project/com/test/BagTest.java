package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.project.Bag;
import com.model.*;

class BagTest {

	Bag bag;
	
	@BeforeEach
	void setUp() throws Exception {
		bag = new Bag();
	}

	@Test
	void testBag() {
		assertTrue(bag.getNumber() == 36);
		Tile tile = new Tile("0WWW1");
		bag.removeTile(tile);
		assertTrue(bag.getNumber() == 35);
		bag.addTile(tile);
		assertTrue(bag.getNumber() == 36);
		assertTrue(bag.getNumber() != 0 && bag.getTile() != null);
		for(int i = 0; i < 5; i++)
			tile = bag.getTile();
		assertTrue(bag.getNumber() < 36);
		bag.recovery();
		assertTrue(bag.getNumber() == 36);
	}

}
