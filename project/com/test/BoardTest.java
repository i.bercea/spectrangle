package com.test;

import com.model.Aspect;
import com.model.Tile;
import com.project.Board;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {
	Board board = new Board();
	Tile tile, tile1, joker;
	Map<Integer, Integer> map = new HashMap<Integer, Integer>();
	
	@BeforeEach
	void setUp() throws Exception {
		board.recovery();
		tile = new Tile("1RAB3");
		tile1 = new Tile("0RAB3");
		joker = new Tile("0WWW1");
	}

	@Test
	void testGetAspect() {
		int upwards[][] = {{0, 0}, {1, -1}, {1, 1}, {2, -2}, {2, 0}, {2, 2},
						   {3, -3}, {3, -1}, {3, 1}, {3, 3}, {4, -4}, {4, -2},
						   {4, 0}, {4, 2}, {4, 4}, {5, -5}, {5, -3}, {5, -1},
						   {5, 1}, {5, 3}, {5, 5}};
		int downwards[][] = {{1, 0}, {2, -1}, {2, 1}, {3, -2}, {3, 0}, {3, 2},
							 {4, -3}, {4, -1}, {4, 1}, {4, 3}, {5, -4}, {5, -2},
							 {5, 0}, {5, 2}, {5, 4}};
		for(int i = 0; i < upwards.length; i++)
			assertTrue(board.getAspect(upwards[i]) == Aspect.upwards);
		for(int i = 0; i < downwards.length; i++)
			assertTrue(board.getAspect(downwards[i]) == Aspect.downwards);
	}
	
	@Test
	void testGetRight() {
		int place[] = {4, 2};
		int place1[] = {4, 1};
		int place2[] = {5, 4};
		board.addTile(tile, place, 0);
		assertTrue(board.getRight(place1).equals(tile));
		assertTrue(board.getRight(place2) == null);
	}
	
	@Test
	void testGetLeft() {
		int place[] = {5, 3};
		int place1[] = {5, 4};
		int place2[] = {5, -4};
		board.addTile(tile, place, 0);
		assertTrue(board.getLeft(place1).equals(tile));
		assertTrue(board.getRight(place2) == null);
	}
	
	@Test
	void testGetTop() {
		int place[] = {1, 1};
		int place1[] = {2, 1};
		int place2[] = {2, 0};
		board.addTile(tile, place, 0);
		assertTrue(board.getTop(place1).equals(tile));
		assertTrue(board.getTop(place2) == null);
	}
	
	@Test
	void testGetBottom() {
		int place[] = {4, 3};
		int place1[] = {3, 3};
		int place2[] = {5, 5};
		board.addTile(tile, place, 0);
		assertTrue(board.getBottom(place1).equals(tile));
		assertTrue(board.getBottom(place2) == null);
	}
	
	@Test
	void testGetBlank() {
		int place[] = {3, 0};
		board.addTile(tile, place, 0);
		List<Integer> temp = board.getBlank();
		assertFalse(temp.contains(place[0]*place[0]+place[0]+place[1]));
		assertTrue(temp.size() == 35);
	}
	
	@Test
	void testGetHighBonuses() {
		List<Integer> temp = Arrays.asList(2, 10, 11, 13, 14, 20, 26, 30, 34);
		assertTrue(board.getHighBonuses().equals(temp)); 
	}
	
	@Test
	void testAddTile() {
		int place[] = {4, 0};
		int place1[] = {5, 2};
		int place2[] = {5, 3};
		assertTrue(board.addTile(tile, place, 0) == -1);
		assertTrue(board.addTile(tile, place1, 0) != -1);
		assertTrue(board.addTile(joker, place2, 0) != -1);
	}
	
	@Test
	void testRemoveTile() {
		int place[] = {0, 0};
		assertTrue(board.addTile(tile, place, 0) != -1);
		board.removeTile(place);
		assertTrue(board.getBoard().get(0) == null);
	}

	@Test
	void testRecovery() {
		int place1[] = {2, -1};
		assertTrue(board.addTile(tile, place1, 0) != -1);
		assertFalse(board.getBoard().isEmpty());
		board.recovery();
		assertTrue(board.getBoard().isEmpty());

	} 
}
