package com.project;

public class Angle {
   
    /**
     * The first permutation of the tile.
     */
	public static final int turn_0 = 0;
	
	/**    
     * The second permutation of the tile
     */
	public static final int turn_120 = 1;
	
	/**
     * The third permutation of the tile
     */
	public static final int turn_240 = 2;
}
