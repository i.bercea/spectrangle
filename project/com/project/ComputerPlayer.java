package com.project;

import java.util.Map;

import com.model.Tile;

public class ComputerPlayer extends Player{
	
    /**
     * Random strategy object
     */
	RandomStrategy randomStrategy = new RandomStrategy();
	
	/**
     * Smart Strategy object
     */
	SmartStrategy smartStrategy = new SmartStrategy();
	
	/**
     *  Use to get the computer player's next move.
     * @param tiles The bag of left tiles.
     * @param map The board.
     * @param i Chosen difficulty (0 for random, 1 for smart)
     * @param firstFlag Signal that this is the first move to be made in the game
     * @return an array of integers representing: the row, the column, the permutation and the index of the chosen tile.
     */
    /*
     * @ pure
     * @ requires tiles != null && map != null && (i == Difficulty.random || i == Difficulty.smart) && (first_flag == true || first_flag == false) ;
     * @ ensures (\result[0] >= 0 && \result[0] <= 5) && (\result[1] >= -5 && \result[1] <= 5) && (\result[2] == 0 || \result[2] == 1 || \result[2] == 2) && (\result[3] >= 0 && \result[3] <= 36) || (\result == null);
     */
	public int[] getPosition(Tile[] tiles, Map<Integer, Tile> map, int i, boolean first_flag) {
		if(i==Difficulty.random) {
			return randomStrategy.getPosition(
					tiles, 
					map,
					first_flag);
		}
		else if(i==Difficulty.smart)
			return smartStrategy.getPosition(
					tiles,
					map,
					first_flag);
		else
			return null;
	}
	
}
