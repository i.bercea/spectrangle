package com.project;

public class Field {
	
	public String getFieldString(Tile[] tiles,boolean flag_sequence) {
		
		String tile_FirstLine = 	"     ʌ     ";
		String tile_SecondLine = 	"    / \\    ";
		String tile_ThirdLine = 	"   / %d \\   ";
		String tile_FourthLine = 	"  /%c   %c\\  ";
		String tile_FifthLine = 	" /   %c   \\ ";
		String tile_SixthLine = 	"/---------\\";
		String tile_SeventhLine = 	"     %d     ";
		
		String tiles_FirstLine = "";
		String tiles_SecondLine = "";
		String tiles_ThirdLine = "";
		String tiles_FourthLine = "";
		String tiles_FifthLine = "";
		String tiles_SixthLine = "";
		String tiles_SeventhLine = 	"";
	    
	    for(int i=0;i<tiles.length;i++) {
	    	tiles_FirstLine = tiles_FirstLine + tile_FirstLine;
	    	tiles_SecondLine = tiles_SecondLine + tile_SecondLine;
	    	tiles_ThirdLine = 
	    			String.format(tiles_ThirdLine + tile_ThirdLine, tiles[i].getValues());
	    	tiles_FourthLine = 
	    			String.format(
	    					tiles_FourthLine + tile_FourthLine,
	    					tiles[i].getLeft(),
	    					tiles[i].getRight());
	    	tiles_FifthLine = 
	    			String.format(tiles_FifthLine + tile_FifthLine, tiles[i].getVertical());
	    	tiles_SixthLine = tiles_SixthLine + tile_SixthLine;
	    	tiles_SeventhLine = 
	    			String.format(tiles_SeventhLine + tile_SeventhLine, i+1);
	    }
	    
	    tiles_FirstLine = tiles_FirstLine + "\n";
		tiles_SecondLine = tiles_SecondLine + "\n";
		tiles_ThirdLine = tiles_ThirdLine + "\n";
		tiles_FourthLine = tiles_FourthLine + "\n";
		tiles_FifthLine = tiles_FifthLine + "\n";
		tiles_SixthLine = tiles_SixthLine + "\n";
		tiles_SeventhLine = tiles_SeventhLine + "\n";
	    
	    if(tiles.length==0)
        	return null;
        else {
        	String template = null;
        	if(flag_sequence)
        		template = 
            		tiles_FirstLine + 
            		tiles_SecondLine + 
            		tiles_ThirdLine + 
            		tiles_FourthLine + 
            		tiles_FifthLine + 
            		tiles_SixthLine +
            		tiles_SeventhLine;
        	else 
        		template = 
    				tiles_FirstLine + 
    				tiles_SecondLine + 
    				tiles_ThirdLine + 
    				tiles_FourthLine + 
    				tiles_FifthLine + 
    				tiles_SixthLine;
        	return template;
        }   
		
	}
	
	public String getTileString(Tile[] tiles,int r,int c) {
		
		String tile_FirstLine = null;
		String tile_SecondLine = null;
		String tile_ThirdLine = null;
		String tile_FourthLine = null;
		String tile_FifthLine = null;
		String tile_SixthLine = null;
		String tile_SeventhLine = null;
		
		if( (r+c)%2 == 0 ) {
			tile_FirstLine = 	"     ʌ     ";
			tile_SecondLine = 	"    / \\    ";
			tile_ThirdLine = 	"   / %d \\   ";
			tile_FourthLine = 	"  /%c   %c\\  ";
			tile_FifthLine = 	" /   %c   \\ ";
			tile_SixthLine = 	"/---------\\";
			tile_SeventhLine = 	"     %d     ";
		}
		else {
			tile_FirstLine = 	"\\---------/";
			tile_SecondLine = 	" \\   %c   / ";
			tile_ThirdLine = 	"  \\%c   %c/  ";
			tile_FourthLine = 	"   \\ %d /   ";
			tile_FifthLine = 	"    \\ /    ";
			tile_SixthLine = 	"     v     ";
			tile_SeventhLine = 	"     %d     ";
		}
		
		String tiles_FirstLine = "";
		String tiles_SecondLine = "";
		String tiles_ThirdLine = "";
		String tiles_FourthLine = "";
		String tiles_FifthLine = "";
		String tiles_SixthLine = "";
		String tiles_SeventhLine = 	"";
	    
	    for(int i=0;i<tiles.length;i++) {
    		tiles_FirstLine = tiles_FirstLine + tile_FirstLine;
	    	tiles_SixthLine = tiles_SixthLine + tile_SixthLine;	 
	    	tiles_SeventhLine = 
	    			String.format(tiles_SeventhLine + tile_SeventhLine, i+1);
	    	if( (r+c)%2 == 0 ) {
		    	tiles_SecondLine = tiles_SecondLine + tile_SecondLine;
		    	tiles_ThirdLine = 
		    			String.format(tiles_ThirdLine + tile_ThirdLine, tiles[i].getValues());
		    	tiles_FourthLine = 
		    			String.format(
		    					tiles_FourthLine + tile_FourthLine,
		    					tiles[i].getLeft(),
		    					tiles[i].getRight());
		    	tiles_FifthLine = 
		    			String.format(tiles_FifthLine + tile_FifthLine, tiles[i].getVertical());
	    	}
	    	else {
	    		tiles_SecondLine = 
	    			String.format(tiles_SecondLine + tile_SecondLine, tiles[i].getVertical());
	    		tiles_ThirdLine = 
		    			String.format(
		    					tiles_ThirdLine + tile_ThirdLine,
		    					tiles[i].getLeft(),
		    					tiles[i].getRight());
	    		tiles_FourthLine = 
		    		String.format(tiles_FourthLine + tile_FourthLine, tiles[i].getValues());
	    		tiles_FifthLine = tiles_FifthLine + tile_FifthLine;   		
	    	}
	    }
	    
	    tiles_FirstLine = tiles_FirstLine + "\n";
		tiles_SecondLine = tiles_SecondLine + "\n";
		tiles_ThirdLine = tiles_ThirdLine + "\n";
		tiles_FourthLine = tiles_FourthLine + "\n";
		tiles_FifthLine = tiles_FifthLine + "\n";
		tiles_SixthLine = tiles_SixthLine + "\n";
		tiles_SeventhLine = tiles_SeventhLine + "\n";
	    
	    String template = 
        		tiles_FirstLine + 
        		tiles_SecondLine + 
        		tiles_ThirdLine + 
        		tiles_FourthLine + 
        		tiles_FifthLine + 
        		tiles_SixthLine +
        		tiles_SeventhLine;
        return template;
	}
}
