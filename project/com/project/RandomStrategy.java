package com.project;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.model.Angle;
import com.model.Tile;

public class RandomStrategy implements Strategy{
	
    /**
     * An auxiliary board
     */
	private static Board virtualPanel = new Board();

	/**
     * Use to get the first possible move.
     * 
     * @param tiles     An array of tiles
     * @param map       The spectrangle board
     * @param firstFlag Flag stating if this is the first move to be made on the
     *                  board
     * @return An array of ints: int[0] is the row, int[1] is the column, int[2] is
     *         the permutation of the tile, int[3] is the index of the tile
     */
    /*
     * @ pure
     * 
     * @ requires tiles != null && map != null && (first_flag == true || first_flag
     * == false);
     * 
     * @ ensures (\result[0] >= 0 && \result[0] <= 5) && (\result[1] >= -5 &&
     * \result[1] <= 5) && (\result[2] == 0 || \result[2] == 1 || \result[2] == 2)
     * && (\result[3] >= 0 && \result[3] <= 36) || (\result == null);
     */
	public int[] getPosition(Tile[] tiles, Map<Integer, Tile> map, boolean first_flag) {
		
		if(map.size()==36)
			return null;
		
		for(int i:map.keySet()) {
			int r = (int) (Math.round((Math.sqrt(i+0.25)-0.5)));
			int c = i - r*r - r;
			int[] place = {r,c};
			virtualPanel.addTile(map.get(i), place, Angle.turn_0);
		}
		
		for(int i=0;i<tiles.length;i++) {
			int[] data = getPosition(tiles[i], map, first_flag);
			if(data==null)
				continue;
			return new int[] {data[0], data[1], data[2], i};
		}
		
		return null;
		
	}
	
	@Override
	/**
     * Use to get the first possible move on the board for a tile.
     * 
     * @param tile      The tile to be placed
     * @param map       The spectrangle board
     * @param firstFlag Flag stating if this is the first move on the board.
     * @return An array of ints: int[1] is the row, int[2] is the column and int[3]
     *         is the permutation of the tile; or null if the tile can't be placed.
     */
    /*
     * @ pure
     * 
     * @ requires tile != null && map != null && (first_flag == false || first_flag
     * == true);
     * 
     * @ ensures (\result[0] >= 0 && \result[0] <= 5) && (\result[1] >= -5 &&
     * \result[1] <= 5) && (\result[2] == 0 || \result[2] == 1 || \result[2] == 2)
     * && (\result[3] >= 0 && \result[3] <= 36) || (\result == null);
     */
	public int[] getPosition(Tile tile, Map<Integer, Tile> map, boolean first_flag) {
		// TODO Auto-generated method stub
		
		if(map.size()==36)
			return null;
		
		List<Integer> data = virtualPanel.getBlank();	
		if(first_flag) {
			Iterator<Integer> it = data.iterator();
			while(it.hasNext()) {
				int temp = it.next();
				if(temp==2)
					it.remove();
				if(temp==10)
					it.remove();
				if(temp==11)
					it.remove();
				if(temp==13)
					it.remove();
				if(temp==14)
					it.remove();
				if(temp==20)
					it.remove();
				if(temp==26)
					it.remove();
				if(temp==30)
					it.remove();
				if(temp==34)
					it.remove();
			}
		}
		
		for(int i:data) {
			int r = (int) (Math.round((Math.sqrt(i+0.25)-0.5)));
			int c = i - r*r - r;
			int[] place = {r,c};
			for(int j=0;j<3;j++) {
				int temp = virtualPanel.addTile(tile, place, j);
				virtualPanel.removeTile(place);
				if(temp!=-1) {
					return new int[] {place[0], place[1], j};
				}
			}
		}
		
		return null;
	}

	
}
