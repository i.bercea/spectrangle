package com.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.model.Angle;
import com.model.PlayListUnit;
import com.model.Tile;

public class Player {
    
    /**
     * An auxiliary board
     */
	private static Board virtualPanel = new Board();
	
	/**
     * The set of tiles in the hand of the player.
     */
	protected Tile[] tiles = new Tile[4];
	
	/**
     * Use to get the tiles currently in the hand of the player
     * 
     * @return An array of tiles, representing the tiles the player currently has in
     *         its hand
     */
    /*
     * @ pure
     * 
     * @ ensures \result.size() >= 0 && \result.size() <= 3;
     * 
     * @ ensures (\for all int i; i >= 0 && i <=3; \result.get(i) != null);
     */
	public Tile[] geTiles() {
		List<Tile> list = new ArrayList<>();
		if(tiles[0]!=null)
			list.add(tiles[0]);
		if(tiles[1]!=null)
			list.add(tiles[1]);
		if(tiles[2]!=null)
			list.add(tiles[2]);
		if(tiles[3]!=null)
			list.add(tiles[3]);
		Tile[] tiles = new Tile[list.size()];
		return list.toArray(tiles);
	}
	
	/**
     * Use to get a map of all possible moves of this player.
     * 
     * @param map The spectrangle board.
     * @return A map whose keySet represents an array of size 2, storing the tile
     *         that can be played (on the first position) and the index of the field
     *         on the board (on the second position); each value is made up of an
     *         array up to size 3 (if all possible permutations of the tile can be
     *         made on that specific field).
     */
    /*
     * @ pure
     * 
     * @ requires map != null;
     */
	public List<PlayListUnit> isPlay(Map<Integer, Tile> map) {
		if(tiles.length==0)
			return null;
		
		for(int i:map.keySet()) {
			int r = (int) (Math.round((Math.sqrt(i+0.25)-0.5)));
			int c = i - r*r - r;
			int[] place = {r,c};
			virtualPanel.addTile(map.get(i), place, Angle.turn_0);
		}
		
		List<Integer> data = virtualPanel.getBlank();
		
		List<PlayListUnit> canPlayList = new ArrayList<>();
		
		for(int i=0;i<4;i++) {
			for(int index:data) {
				for(int j=0;j<3;j++) {
					if(tiles[i]!=null) {
						int r = (int) (Math.round((Math.sqrt(index+0.25)-0.5)));
						int c = index - r*r - r;
						int[] place = {r,c};
						Tile tile_temp = tiles[i].setAspect(place);
						if(virtualPanel.addTile(tile_temp, place, j)!=-1) {
							virtualPanel.removeTile(place);
							canPlayList.add(new PlayListUnit(i, index, tile_temp.turn(j).toEncoding()));
						}
					}
				}
			}
		}
		
		return canPlayList;
	}
	
	/**
     * Use to add a tile to the player's hand.
     * 
     * @param tile The tile extracted from the bag, to be put in the player's hand.
     * @return True if the tile has been successfully put in the player's hand,
     *         otherwise false (player's hand is full);
     */
    /*
     * @ requires tile != null;
     * 
     * @ ensures (\result == true && tiles[0] != null) || (\result == false &&
     * tiles[3] != null);
     */
	public boolean addTile(Tile tile) {
		if (tiles[0]==null) {
			tiles[0] = tile;
			return true;
		}
		else if (tiles[1]==null) {
			tiles[1] = tile;
			return true;
		}
		else if (tiles[2]==null) {
			tiles[2] = tile;
			return true;
		}
		else if (tiles[3]==null) {
			tiles[3] = tile;
			return true;
		}
		else
			return false;		
	}
	
	/**
     * Use to remove a tile from the player's hand.
     * 
     * @param i The index of the tile to be taken from the player's hand
     * @return true if the tile has been successfully removed, false otherwise.
     */
    /*
     * @ requires i >= 0 && i <= 3;
     * 
     * @ ensures (\result == true && tiles[i] == null) || \result == false;
     */
	public boolean removeTile(int i) {
		if(i>=0&&i<=3) {
			tiles[i] = null;
			return true;
		}
		else
			return false;
	}
	
	/**
     * Use to remove a tile from the player's hand.
     * 
     * @param tile The tile to be removed from the player's hand.
     * @return true if the tile has been successfully removed, otherwise false.
     */
    /*
     * @ requires tile != null;
     * 
     * @ ensures (\result == true && (\forall i; i >= 0 && i <= 3;
     * !tiles.get(i).equals(tile))) || (\result == false);
     */
	public boolean removeTile(Tile tile) {
		if(tiles[0].equals(tile)) {
			tiles[0] = null;
			return true;
		}
		else if(tiles[1].equals(tile)) {
			tiles[1] = null;
			return true;
		}else if(tiles[2].equals(tile)) {
			tiles[2] = null;
			return true;
		}else if(tiles[3].equals(tile)) {
			tiles[3] = null;
			return true;
		}
		else
			return false;
	}
}
