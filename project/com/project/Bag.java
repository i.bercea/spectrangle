package com.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.model.Tile;

import java.util.Random;

public class Bag {
	
    /**
     * List of all existing tiles.
     */
	private List<Tile> tiles_all = new ArrayList<>();
	
	/**
     * The bag of tiles.
     */
	private Map<Integer,Tile> tiles = new HashMap<>();
	
	public Bag() {
		tiles_all.add(new Tile("0RRR6"));
		tiles_all.add(new Tile("0GGG6"));
		tiles_all.add(new Tile("0BBB6"));
		tiles_all.add(new Tile("0YYY6"));
		tiles_all.add(new Tile("0PPP6"));
		
		tiles_all.add(new Tile("0RRY5"));
		tiles_all.add(new Tile("0RRP5"));
		tiles_all.add(new Tile("0BBR5"));
		tiles_all.add(new Tile("0BBP5"));
		tiles_all.add(new Tile("0GGR5"));
		tiles_all.add(new Tile("0GGB5"));
		tiles_all.add(new Tile("0YYG5"));
		tiles_all.add(new Tile("0YYB5"));
		tiles_all.add(new Tile("0PPY5"));
		tiles_all.add(new Tile("0PPG5"));
		
		tiles_all.add(new Tile("0RRB4"));
		tiles_all.add(new Tile("0RRG4"));
		tiles_all.add(new Tile("0BBG4"));
		tiles_all.add(new Tile("0BBY4"));
		tiles_all.add(new Tile("0GGY4"));
		tiles_all.add(new Tile("0GGP4"));
		tiles_all.add(new Tile("0YYR4"));
		tiles_all.add(new Tile("0YYP4"));
		tiles_all.add(new Tile("0PPR4"));
		tiles_all.add(new Tile("0PPB4"));
		
		tiles_all.add(new Tile("0YBP3"));
		tiles_all.add(new Tile("0RGY3"));
		tiles_all.add(new Tile("0BGP3"));
		tiles_all.add(new Tile("0GRB3"));
		
		tiles_all.add(new Tile("0BRP2"));
		tiles_all.add(new Tile("0YPR2"));
		tiles_all.add(new Tile("0YPG2"));
		
		tiles_all.add(new Tile("0GRP1"));
		tiles_all.add(new Tile("0BYG1"));
		tiles_all.add(new Tile("0RYB1"));
		
		tiles_all.add(new Tile("0WWW1"));		
		
		for(int i=0;i<36;i++)
			tiles.put(i, tiles_all.get(i));
	}
	
	/**
     * Use to get the number of tiles currently in the bag.
     * 
     * @return the number of tiles in the bag
     */
    /*
     * @ pure
     * 
     * @ ensures \result >= 0 && \result <= 36;
     */
	public int getNumber() {
		return tiles.size();
	}
	
	/**
     * Use to get a random tile from the bag.
     * 
     * @return a random Tile from the bag
     */
    /*
     * @ ensures old(getNumber()) == getNumber + 1;
     */
	public Tile getTile() {
		if(tiles.isEmpty())
			return null;
		Integer[] keys = tiles.keySet().toArray(new Integer[0]);
		Random random = new Random();
		Integer randomKey = keys[random.nextInt(keys.length)];
		Tile temp = tiles.get(randomKey);
		tiles.remove(randomKey);
		return temp;
	}
	
	/**
     * Use to put a tile in the bag.
     * 
     * @param tile The tile to be added in the bag
     * @return true if the tile has been successfully added, otherwise false
     */
    /*
     * @ requires tile != null;
     * 
     * @ ensures \result == false || (\result == true && getNumber() ==
     * old(getNumber()) + 1);
     */
	public boolean addTile(Tile tile) {
		if(tiles.containsValue(tile))
			return false;
		else {
			for(int i=0;i<tiles_all.size();i++) {
				if(tile.equals(tiles_all.get(i))) {
					tiles.put(i, tile);
					return true;
				}
			}
			return false;
		}
	}
	
	/**
     * Use to remove a tile from the bag.
     * 
     * @param tile The tile to be removed from the bag
     * @return true if the tile has been successfully removed, otherwise false
     */
    /*
     * @ requires tile != null;
     * 
     * @ ensures \result == false || (\result == true && getNumber + 1 =
     * old(getNumber()));
     */
	public boolean removeTile(Tile tile) {
		if(tiles.containsValue(tile)) {
			for(Iterator<Entry<Integer, Tile>> it = tiles.entrySet().iterator();it.hasNext();) {
				Entry<Integer, Tile> entry = it.next();
				if(tile.equals(entry.getValue()))
					it.remove();
			}
			return true;
		}
		else
			return false;
	}
	
	/**
     * Reestablishes the bag to its original state.
     */
	public void recovery() {
		for(Iterator<Entry<Integer, Tile>> it = tiles.entrySet().iterator();it.hasNext();) {
			it.next();
			it.remove();
		}
		for(int i=0;i<36;i++)
			tiles.put(i, tiles_all.get(i));
	}
	
	/**
	 * Use to get the bag of tiles.
	 * @return A map representing the bag of tiles.
	 */
	public Map<Integer, Tile> getTiles() {
        return this.tiles;
    }
}
