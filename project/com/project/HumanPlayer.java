package com.project;

import com.model.Tile;

public class HumanPlayer extends Player{
    
    /**
     * The name of the player
     */
	private String username = null;
	
	/**
     * The score of the player
     */
	private int score = 0;
	
	/**
     * The number of points the first card in the player's hand is worth
     */
	private int FirstCardValue = 0;

	/**
     * Use to get the name of this human player.
     * 
     * @return The name of this human player.
     */
	public String getUsername() {
		return username;
	}

	/**
     * Use to set the name of this human player.
     * 
     * @param username The name this human player should have.
     */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
     * Use to set set a tile as the first one in the player's hand.
     * 
     * @param tile The tile to be set
     * @return true if the tile has been successfully put in the player's hand,
     *         otherwise false
     */
    /*
     * @ requires tile != null;
     * 
     * @ ensures (\result == true && tiles[0] != null) || (\result == false &&
     * tiles[3] != null);
     */
	public boolean setFirstCard(Tile tile) {
		FirstCardValue = tile.getValues();
		if (tiles[0]==null) {
			tiles[0] = tile;
			return true;
		}
		else if (tiles[1]==null) {
			tiles[1] = tile;
			return true;
		}
		else if (tiles[2]==null) {
			tiles[2] = tile;
			return true;
		}
		else if (tiles[3]==null) {
			tiles[3] = tile;
			return true;
		}
		else
			return false;	
	}
	
	/**
     * Use to get the points the first tile in the player's hand is worth.
     * 
     * @return the points the first tile in the player's hand is worth.
     */
    /*
     * @ pure
     * 
     * @ ensures \result >= 1 && \result <= 6;
     */
	public int getFirstCardValue() {
		return FirstCardValue;
	}

	/**
     * Use to get the score of the player.
     * 
     * @return The number of points the player has.
     */
    /*
     * @ pure
     * 
     * @ ensures \result >= 0;
     */
	public int getScore() {
		return score;
	}

	/**
     * Use to set the score of this player to a value.
     * 
     * @param score The number of points this player has
     */
    /*
     * @ requires score >= 0;
     * 
     * @ ensures \result == score;
     */
	public void setScore(int score) {
		this.score = score;
	}
	
} 
