package com.project;

import java.util.Map;

import com.model.Tile;

public interface Strategy {
    
    /**
     * Use to get the next move. (random or smart)
     * 
     * @param tile      The tile to be placed
     * @param map       The spectrangle board
     * @param firstFlag Flag stating if this is the first move to be made on the
     *                  board.
     * @return an array of ints: int[0] the row, int[1] the column, int[2] the
     *         permutation of the tile and int[3] the field index where the tile has
     *         to be placed
     */
    /*
     * @ pure
     * 
     * @ requires tile != null && map != null && (first_flag == false || first_flag
     * == true);
     * 
     * @ ensures (\result[0] >= 0 && \result[0] <= 5) && (\result[1] >= -5 &&
     * \result[1] <= 5) && (\result[2] == 0 || \result[2] == 1 || \result[2] == 2)
     * && (\result[3] >= 0 && \result[3] <= 36) || (\result == null);
     */
	public int[] getPosition(Tile tile, Map<Integer, Tile> map, boolean first_flag);
}
