package com.project;

public class Aspect {
    
    /**
     * The field facing upwards
     */
	public static final int upwards = 0;
	
	/**
     * The field facing downwards
     */
	public static final int downwards = 1;
}
