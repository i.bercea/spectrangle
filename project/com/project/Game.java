package com.project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.model.Angle;
import com.model.Field;
import com.model.PlayListUnit;
import com.model.Tile;
import com.network.Client;

public class Game {

    /**
     * The field object (will print the permutations of a tile) and the tiles in a
     * player's hand.
     */
	static Field field = new Field();
	
	/**
     * The spectrangle board
     */
	static Board board = new Board();
	
	/**
     * The bag of tiles
     */
	static Bag bag = new Bag();
	
	/**
     * Human player
     */
	static Player I = new Player();
	
	/**
     * Computer player
     */
	static ComputerPlayer computerPlayer = new ComputerPlayer();
	
	/**
     * Human player's score
     */
	static int I_score = 0;
	
	/**
     * Computer player's score
     */
	static int It_score = 0;
	
	/**
     * Human player's input
     */
	static Scanner scanner = new Scanner(System.in);
	
	private static void start() throws Exception {
		I_score = 0;
		It_score = 0;
		System.out.print("Please select the game mode:\n"
				+ "1. Single game.\n"
				+ "2. Online game.");	
		while(true) {			
			int select = 0;
			try {
				select = new Integer(scanner.next());
				if(select==1) {
					single();
					break;
				}
				else if(select==2) {
					online();
					break;
				}
				else {
					System.out.print("\nThe number entered is not in the list!" +
							"\nPlease re-select the game mode:\n"
							+ "1. Single game.\n"
							+ "2. Online game.");	
					continue;
				}
			} catch (NumberFormatException e) {
				// TODO: handle exception
				System.out.print("\nThe input is not a number!" +
						"\nPlease re-select the game mode:\n"
						+ "1. Single game.\n"
						+ "2. Online game.");
				continue;
			}
		}
	}

	/**
     * Single game
     */
	private static void single() {
		System.out.print("\nPlease select the difficulty of the game:\n"
				+ "1. Random.\n"
				+ "2. Smart.");	
		while(true) {			
			int select = 0;
			try {
				select = new Integer(scanner.next());
				if(select==1) {
					
					while(true) {	
						I_score = 0;
						It_score = 0;
						try {
							computer(Difficulty.random);
							break;
						} catch (NumberFormatException e) {
							// TODO: handle exception
							System.out.println("\nThe input is not a number!\nThe game will start again!\n");
							bag.recovery();
							board.recovery();
							continue;
						}
					}
					
					break;
				}
				else if(select==2) {

					while(true) {	
						I_score = 0;
						It_score = 0;
						try {
							computer(Difficulty.smart);
							break;
						} catch (NumberFormatException e) {
							// TODO: handle exception
							System.out.println("\nThe input is not a number!");
							bag.recovery();
							board.recovery();
							continue;
						}
					}
					
					break;
				}
				else {
					System.out.print("\nThe number entered is not in the list!" +
							"\nPlease re-select the difficulty of the game:\n"
							+ "1. Random.\n"
							+ "2. Smart.");	
					continue;
				}
			} catch (NumberFormatException e) {
				// TODO: handle exception
				System.out.print("\nThe input is not a number!" +
						"\nPlease re-select the difficulty of the game:\n"
						+ "1. Random.\n"
						+ "2. Smart.");
				continue;
			}
		}
	}
	
	/**
     * Online game
     */
	private static void online() {
		System.out.print("\nPlease enter the server's IP:");
		String host = null;
		while(true) {
			host = scanner.next();
			if(host.matches("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$"))
				break;
			else
				System.out.print("The IP format is incorrect. Please re-enter the server's IP:");
		}
		Client client = new Client(host, field, board, scanner);
		client.start();	
		
	}
	
	/**
     * Use to check if the game is over. Checks that the bag is empty and that none
     * of the players can't make any more moves.
     * 
     * @return true if the game is over, otherwise false.
     */
    /*
     * @ pure
     * 
     * @ ensures \result == true || \result == false;
     */
	private static boolean endGameJudge() {
		return (bag.getNumber()==0) && (
			((I.isPlay(board.getBoard())==null)||(I.isPlay(board.getBoard()).size()==0)) &&
			((computerPlayer.isPlay(board.getBoard())==null)||(computerPlayer.isPlay(board.getBoard()).size()==0))
			);
	}
	
	/**
     * Use to make a move as the Human player
     * 
     * @param first_flag Flag stating that this is the first move to be made on the
     *                   board
     * @throws NumberFormatException Exception thrown when the user enters a wrong
     *                               input in the terminal.
     */
    /*
     * @ requires first_flag == false || first_flag == true;
     */
	private static void i_Order(boolean first_flag) throws NumberFormatException{
		System.out.println(board.getBoardString());
		System.out.printf("Opponents tiles(%d):\n",It_score);
		System.out.println(field.getFieldString(computerPlayer.geTiles(), true));
		System.out.printf("Your tiles(%d):\n",I_score);
		System.out.println(field.getFieldString(I.geTiles(), true));
		
		List<PlayListUnit> canPlayList = I.isPlay(board.getBoard());	
		
		List<Integer> tileList = new ArrayList<>();
		List<Integer> boardList = new ArrayList<>();
		
		for(PlayListUnit playListUnit:canPlayList) {
			if(!tileList.contains(playListUnit.getTile_index()))
				tileList.add(playListUnit.getTile_index());
		}
		
		if((canPlayList!=null)&&(canPlayList.size()!=0)) {
			System.out.printf("Choose which tile to play [1..%d]:",I.geTiles().length);
			int tile_index = -1;
			int tile_row = -1;
			int tile_column = -1;
			while(true) {
				tile_index = new Integer(scanner.next()) - 1;
				if(tile_index>=I.geTiles().length||tile_index<0)
					System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
				else{
					if(tileList.contains(tile_index))
						break;
					else
						System.out.print("\nThe tile you chose can't play!\nPlease re-select!\n");
				}
			}
			for(PlayListUnit playListUnit:canPlayList) {
	    		if(playListUnit.getTile_index()==tile_index)
	    			if(!boardList.contains(playListUnit.getBoard_index()))
	    				boardList.add(playListUnit.getBoard_index());
			}
			while(true) {
				System.out.print("Choose which row to play [1..6]:");
				while((tile_row = new Integer(scanner.next()) - 1)>5||tile_row<0)
					System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
				System.out.printf("Choose which column to play: [%d..%d]:",-1*tile_row,tile_row);
				while((tile_column = new Integer(scanner.next()))>tile_row||tile_column<-tile_row)
					System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
				if(first_flag) {
					if(
							(boardList.contains(tile_row*tile_row+tile_row+tile_column))&&
							(!board.getHighBonuses().contains(tile_row*tile_row+tile_row+tile_column)))
						break;
					else
						System.out.print("\nThe position you chose can't play!\nPlease re-select!\n");
				}
				else {
					if(boardList.contains(tile_row*tile_row+tile_row+tile_column))
						break;
					else
						System.out.print("\nThe position you chose can't play!\nPlease re-select!\n");
				}
			}
			Tile[] tiles = new Tile[0];
			for(PlayListUnit playListUnit:canPlayList) {
	    		if(playListUnit.getTile_index()==tile_index)
	    			if(playListUnit.getBoard_index()==(tile_row*tile_row+tile_row+tile_column)) {
	    				tiles = Arrays.copyOf(tiles, tiles.length+1);
	    				tiles[tiles.length-1] = new Tile(playListUnit.getTile_encoding());
	    			}
			}
			
			System.out.println(
					field.getTileString(tiles, tile_row, tile_column));
			System.out.printf(
					"Choose which permutation to play [%d..%d]: ", 1, tiles.length);
			int tile_angle = -1;
			while((tile_angle = new Integer(scanner.next()) - 1)>tiles.length-1||tile_angle<0)
				System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
			int[] place = new int[2];
			place[0] = tile_row;
			place[1] = tile_column;
			if(first_flag)
				I_score = board.addTile(tiles[tile_angle], place, Angle.turn_0);
			else
				I_score = I_score + board.addTile(tiles[tile_angle], place, Angle.turn_0);
			I.removeTile(tile_index);
			I.addTile(bag.getTile());
			
		}
		else {
			System.out.println("\nYou can't play tiles in your hands.You can:\n1.skip.\n2.Exchange and skip.");
			int index = 0;
			while((index = new Integer(scanner.next()))>2||index<1)
				System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
			if (index==2) {
				System.out.printf("Choose which tile to put back [1..%d]:",I.geTiles().length);
				int tile_index = -1;
				while(true) {
					tile_index = new Integer(scanner.next()) - 1;
					if(tile_index>=I.geTiles().length||tile_index<0)
						System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
					else
						break;
				}
				bag.addTile(I.geTiles()[tile_index]);
				I.removeTile(tile_index);
				I.addTile(bag.getTile());
			}
		}
		
	}
	
	/**
     * Use to make a move with the Computer player
     * 
     * @param difficulty The difficulty of the game (random or smart)
     * @param first_flag Flag stating that this is the first move to be made on the
     *                   board
     */
    /*
     * @ requires (difficulty == Difficulty.random || difficulty ==
     * Difficulty.smart) && (first_flag == false || first_flag == true);
     */
	private static void computer_Order(int difficulty, boolean first_flag) {
		int[] position = 
				computerPlayer.getPosition(
						computerPlayer.geTiles(), 
						board.getBoard(), 
						difficulty,
						first_flag);
		if(position==null) {
			int index = (int)((Math.random()*10)%computerPlayer.geTiles().length);
			bag.addTile(computerPlayer.geTiles()[index]);
			computerPlayer.removeTile(index);
			computerPlayer.addTile(bag.getTile());
		}
		else {
			if(first_flag)
				It_score = board.addTile(
						computerPlayer.geTiles()[position[3]], 
						new int[] {position[0], position[1]}, 
						position[2]);
			else
				It_score = It_score + board.addTile(
					computerPlayer.geTiles()[position[3]], 
					new int[] {position[0], position[1]}, 
					position[2]);
			computerPlayer.removeTile(position[3]);
			computerPlayer.addTile(bag.getTile());
		}
	}
	
	/**
     * Main frame of the game versus a computer player
     * 
     * @param difficulty The difficulty of the computer player (random or smart).
     */
    /*
     * @ requires difficulty == Difficulty.random || difficulty == Difficulty.smart;
     */
	private static void computer(int difficulty) throws NumberFormatException{
		I_score = 0;
		It_score = 0;
		
		I.addTile(bag.getTile());
		computerPlayer.addTile(bag.getTile());
		if(I.geTiles()[0].getValues() >= computerPlayer.geTiles()[0].getValues()) {
			System.out.println("\nYou get bigger chess pieces. You play first.");
			I.addTile(bag.getTile());
			computerPlayer.addTile(bag.getTile());
			I.addTile(bag.getTile());
			computerPlayer.addTile(bag.getTile());
			I.addTile(bag.getTile());
			computerPlayer.addTile(bag.getTile());
			
			i_Order(true);			
			
			while(!endGameJudge()) {
				computer_Order(difficulty, false);
				i_Order(false);
			}
			if(I_score>It_score)
				System.out.println("\nYou win");
			else if(It_score>I_score)
				System.out.println("\nRobot win");
			else
				System.out.println("\nThe game was tied.");
		}
		else {
			System.out.println("\nRobot get bigger chess pieces. Robot play first.");
			I.addTile(bag.getTile());
			computerPlayer.addTile(bag.getTile());
			I.addTile(bag.getTile());
			computerPlayer.addTile(bag.getTile());
			I.addTile(bag.getTile());
			computerPlayer.addTile(bag.getTile());
			
			computer_Order(difficulty, true);
			
			while(!endGameJudge()) {
				i_Order(false);
				computer_Order(difficulty, false);
			}
			if(I_score>It_score)
				System.out.println("\nYou win");
			else if(It_score>I_score)
				System.out.println("\nRobot win");
			else
				System.out.println("\nThe game was tied.");
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		start();		
	}
	
}
