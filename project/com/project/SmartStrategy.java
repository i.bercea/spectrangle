package com.project;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.model.Angle;
import com.model.Tile;

public class SmartStrategy implements Strategy{

    /**
     * An auxiliary board
     */
	private static Board virtualPanel = new Board();
	
	/**
     * Use to get the next smart move.
     * 
     * @param tiles     The set of tiles.
     * @param map       The spectrangle board.
     * @param firstFlag Flag stating if this is the first move to be made on the
     *                  board.
     * @return An array of ints: int[0] the row, int[1] the column, int[2] the
     *         permutation of the tile, int[3] the index of the tile
     */
    /*
     * @ pure
     * 
     * @ requires tiles != null && map != null && (first_flag == true || first_flag
     * == false);
     * 
     * @ ensures (\result[0] >= 0 && \result[0] <= 5) && (\result[1] >= -5 &&
     * \result[1] <= 5) && (\result[2] == 0 || \result[2] == 1 || \result[2] == 2)
     * && (\result[3] >= 0 && \result[3] <= 36) || (\result == null);
     */
	public int[] getPosition(Tile[] tiles, Map<Integer, Tile> map, boolean first_flag) {
		
		if(map.size()==36)
			return null;
		
		int score = 0;
		int[] position = null;
		int angle = -1;
		int sequence = -1;
		
		for(int i:map.keySet()) {
			int r = (int) (Math.round((Math.sqrt(i+0.25)-0.5)));
			int c = i - r*r - r;
			int[] place = {r,c};
			virtualPanel.addTile(map.get(i), place, Angle.turn_0);
		}
		
		for(int i=0;i<tiles.length;i++) {
			int[] data = getPosition(tiles[i], map, first_flag);
			if(data==null)
				continue;
			int[] place = {data[0], data[1]};
			int temp = virtualPanel.addTile(tiles[i], place, data[2]);
			virtualPanel.removeTile(place);
			if(temp!=-1&&score<temp) {
				score = temp;
				position = data;
				sequence = i;
				angle = data[2];
			}
		}
		
		if(score==0&&position==null&&angle==-1)
			return null;
		
		return new int[]{position[0], position[1], position[2], sequence};
		
	}
	
	@Override
	/**
     * Use to get the best (smart) move for a tile.
     * 
     * @param tile      The tile to be placed
     * @param map       The spectrangle board
     * @param firstFlag Flag stating that this is the first move to be made on the
     *                  board
     * @return An array of ints: int[0] is the row, int[1] is the column and int[2]
     *         is the permutation of the tile
     */
    /*
     * @ pure
     * 
     * @ requires tile != null && map != null && (first_flag == false || first_flag
     * == true);
     * 
     * @ ensures (\result[0] >= 0 && \result[0] <= 5) && (\result[1] >= -5 &&
     * \result[1] <= 5) && (\result[2] == 0 || \result[2] == 1 || \result[2] == 2)
     * && (\result[3] >= 0 && \result[3] <= 36) || (\result == null);
     */
	public int[] getPosition(Tile tile, Map<Integer, Tile> map, boolean first_flag) {
		// TODO Auto-generated method stub
		if(map.size()==36)
			return null;
		
		int score = 0;
		int position = -1;
		int angle = -1;
		
		List<Integer> data = virtualPanel.getBlank();
		if(first_flag) {
			Iterator<Integer> it = data.iterator();
			while(it.hasNext()) {
				int temp = it.next();
				if(temp==2)
					it.remove();
				if(temp==10)
					it.remove();
				if(temp==11)
					it.remove();
				if(temp==13)
					it.remove();
				if(temp==14)
					it.remove();
				if(temp==20)
					it.remove();
				if(temp==26)
					it.remove();
				if(temp==30)
					it.remove();
				if(temp==34)
					it.remove();
			}
		}
		
		for(int i:data) {
			int r = (int) (Math.round((Math.sqrt(i+0.25)-0.5)));
			int c = i - r*r - r;
			int[] place = {r,c};
			for(int j=0;j<3;j++) {
				int temp = virtualPanel.addTile(tile, place, j);
				virtualPanel.removeTile(place);
				if(temp!=-1&&score<temp) {
					score = temp;
					position = i;
					angle = j;
				}
			}
		}
		
		if(score==0&&position==-1&&angle==-1)
			return null;
		
		int r = (int) (Math.round((Math.sqrt(position+0.25)-0.5)));
		int c = position - r*r - r;
		int[] place = {r,c,angle};
		return place;
	}

}
