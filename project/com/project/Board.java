package com.project;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Stream;

import com.model.Aspect;
import com.model.Tile;

public class Board {
    
    /**
     * The list of bonuses
     */
    private static List<Integer> bonuses =     Arrays.asList(1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 2, 4, 1, 4, 2, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 3, 1);
    
    /**
     * The list of values
     */
    private static List<Integer> values =    Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35);
    
    /**
     * The list of neighbouring colors on the top or the bottom
     */
    private static List<Character> vertical =  Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    
    /**
     * The list of neighbouring colors on the left
     */
    private static List<Character> left =      Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    
    /**
     * The list of neighbouring colors on the right
     */
    private static List<Character> right =     Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    
    /**
     * The spectrangle board
     */
    private static Map<Integer, Tile> datas = new HashMap<>();
    
    /**
    * Use to determine the aspect of a field.
    * 
    * @param place An array of length 2, with place[0] being the row and place[1]
    *              being the column of a field
    * @return 0 if the field is facing upwards, 1 if the field is facing downwards
    */
   // @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
   // @ ensures \result == Aspect.upwards || \result == Aspect.downwards;
    public int getAspect(int[] place) {     
        if ( (place[0] + place[1]) % 2 == 0)
            return Aspect.upwards;
        else
            return Aspect.downwards;
    }
    
    /**
    * Use to get the right neighbour of a field.
    * 
    * @param place An array of length 2, with place[0] being the row and place[1]
    *              being the column of a field
    * @return a Tile, the right neighbour of the field at (place[0], place[1]),
    *         otherwise null
    */
   // @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
    public Tile getRight(int[] place) {
        int r = place[0];
        int c = place[1];
        if( (c + 1) <= r )
            return datas.get(r*r+r+(c+1));
        else
            return null;
    }
    
    /**
     * Use to get the left neighbour of a field.
     * 
     * @param place An array of length 2, with place[0] being the row and place[1]
     *              being the column of a field
     * @return a Tile, the left neighbour of the field at (place[0], place[1]),
     *         otherwise null
     */
    // @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
    public Tile getLeft(int[] place) {
        int r = place[0];
        int c = place[1];
        if( (c - 1) >= (0 - r) )
            return datas.get(r*r+r+(c-1));
        else
            return null;
    }
    
    /**
     * Use to get the top neighbour of a field.
     * 
     * @param place An array of length 2, with place[0] being the row and place[1]
     *              being the column of a field
     * @return a Tile, the top neighbour of the field at (place[0], place[1]),
     *         otherwise null
     */
    // @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
    public Tile getTop(int[] place) {
        int r = place[0];
        int c = place[1];
        if( getAspect(place) == Aspect.downwards )
            return datas.get((r-1)*(r-1)+(r-1)+c);
        else
            return null;
    }
    
    /**
     * Use to get the bottom neighbour of a field.
     * 
     * @param place An array of length 2, with place[0] being the row and place[1]
     *              being the column of a field
     * @return a Tile, the bottom neighbour of the field at (place[0], place[1]),
     *         otherwise null
     */
    // @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
    public Tile getBottom(int[] place) {
        int r = place[0];
        int c = place[1];
        if( getAspect(place) == Aspect.upwards && (r+1) <= 5 )
            return datas.get((r+1)*(r+1)+(r+1)+c);
        else
            return null;
    }
    
    /**
     * Use to get the list of indexes of fields that do not have a tile on it.
     * 
     * @return a list of integers representing the fields that don't a set tile
     */
    // @ ensures (\forall int i; i >= 0 && i < \result.size(); \result.get(i) ==
    // null);
    public List<Integer> getBlank() {
        List<Integer> temp = new ArrayList<>();
        for(int i=0;i<left.size();i++) {
            if(left.get(i)==null)
                temp.add(i);
        }
        return temp;
    }
    
    /**
     * Use to get the fields with bonuses.
     * 
     * @return a list of integers representing the indexes of fields at which there
     *         are bonuses
     */
    // @ pure
    public List<Integer> getHighBonuses() {
        return Arrays.asList(2, 10, 11, 13, 14, 20, 26, 30, 34);
    }
    
    /**
     * Use to add a tile on the board. There are 3 angles to choose from: TURN_0,
     * TURN_120 and TURN_240.
     * 
     * @param tile  The tile to be set on the spectrangle board
     * @param place An array of length 2, with place[0] being the row and place[1]
     *              being the column of a field
     * @param angle The permutation of the tile to be used
     * @return the number of points obtained by placing the tile, otherwise -1
     */
    /*
     * @ requires tile != null;
     * 
     * @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
     * 
     * @ requires angle == Angle.turn_0 || angle == Angle.turn_120 ||
     * Angle.turn_240;
     * 
     * @ ensures \result = -1 || \result >= 1;
     */
    public int addTile(Tile tile,int place[],int angle) {
        int r = place[0];
        int c = place[1];
        if(datas.containsKey(r*r+r+c)||(datas.isEmpty()&&(bonuses.get(r*r+r+c)!=1)))
            return -1;
        tile = tile.turn(angle);
        tile.setAspect(place);
        int edges = 0;
        if(getAspect(place) == Aspect.upwards) {
            
            if(tile.getLeft()=='W'&&tile.getRight()=='W'&&tile.getVertical()=='W'&&(getLeft(place)!=null||getRight(place)!=null||getBottom(place)!=null))
                edges = 1;
            else {
                if(getLeft(place)!=null&&(!(tile.getLeft().equals(getLeft(place).getRight())||getLeft(place).getRight().equals('W'))))
                    return -1;
                if(getRight(place)!=null&&(!(tile.getRight().equals(getRight(place).getLeft())||getRight(place).getLeft().equals('W'))))
                    return -1;
                if(getBottom(place)!=null&&(!(tile.getVertical().equals(getBottom(place).getVertical())||getBottom(place).getVertical().equals('W'))))
                    return -1;
                
                if(getLeft(place)!=null&&(tile.getLeft().equals(getLeft(place).getRight())||getLeft(place).getRight().equals('W')))
                    edges = edges + 1;
                if(getRight(place)!=null&&(tile.getRight().equals(getRight(place).getLeft())||getRight(place).getLeft().equals('W')))
                    edges = edges + 1;
                if(getBottom(place)!=null&&(tile.getVertical().equals(getBottom(place).getVertical())||getBottom(place).getVertical().equals('W')))
                    edges = edges + 1;
            }
            
            if(datas.isEmpty()||edges>0) {
                datas.put(r*r+r+c, tile);
                left.set(r*r+r+c, tile.getLeft());
                right.set(r*r+r+c, tile.getRight());
                vertical.set(r*r+r+c, tile.getVertical());
                return tile.getValues()*Math.max(1, edges)*bonuses.get(r*r+r+c);
            }
            else
                return -1;
                
        }
        else {
            
            if(tile.getLeft()=='W'&&tile.getRight()=='W'&&tile.getVertical()=='W'&&(getLeft(place)!=null||getRight(place)!=null||getTop(place)!=null))
                edges = 1;
            else {
                if(getLeft(place)!=null&&(!(tile.getLeft().equals(getLeft(place).getRight())||getLeft(place).getRight().equals('W'))))
                    return -1;
                if(getRight(place)!=null&&(!(tile.getRight().equals(getRight(place).getLeft())||getRight(place).getLeft().equals('W'))))
                    return -1;
                if(getTop(place)!=null&&(!(tile.getVertical().equals(getTop(place).getVertical())||getTop(place).getVertical().equals('W'))))
                    return -1;
                
                if(getLeft(place)!=null&&(tile.getLeft().equals(getLeft(place).getRight())||getLeft(place).getRight().equals('W')))
                    edges = edges + 1;
                if(getRight(place)!=null&&(tile.getRight().equals(getRight(place).getLeft())||getRight(place).getLeft().equals('W')))
                    edges = edges + 1;
                if(getTop(place)!=null&&(tile.getVertical().equals(getTop(place).getVertical())||getTop(place).getVertical().equals('W')))
                    edges = edges + 1;
            }
            
            if(datas.isEmpty()||edges>0) {
                datas.put(r*r+r+c, tile);
                left.set(r*r+r+c, tile.getLeft());
                right.set(r*r+r+c, tile.getRight());
                vertical.set(r*r+r+c, tile.getVertical());
                return tile.getValues()*Math.max(1, edges)*bonuses.get(r*r+r+c);
            }
            else
                return -1;
            
        }   
    }

    /**
     * Use to remove a tile from the board.
     * 
     * @param place An array of length 2, with place[0] being the row and place[1]
     *              being the column of a field
     * @return true if the tile has been successfully removed, otherwise false
     */
    /*
     * @ requires place[0] >= 0 && place[0] <= 5 && place[1] >= -5 && place[1] <= 5;
     * 
     * @ ensures \result == true || \result == false;
     */
    public boolean removeTile(int[] place) {
        int r = place[0];
        int c = place[1];
        int index = r*r + r + c;
        if(datas.containsKey(index)) {
            datas.remove(index);
            left.set(index, null);
            right.set(index, null);
            vertical.set(index, null);
            return true;
        }
        else
            return false;
    }
    
    /**
     * Method to print a board of Spectrangle, given the properties of the pieces that reside on it.
     *
     * The arguments to this method together represent the current state of the board. You will need to generate
     * these arguments yourself. Each argument needs to be a list of exactly 36 items, representing the 36 fields
     * on your board. The index in the list corresponds with the index of the field on the board, as they were
     * explained in the slides of the Project Design session in week 6.
     *
     * An example of how you would generate the 'values' list is as follows, assuming your board has a List of fields,
     * which have an attribute called "value" to represent the value of the piece that is placed in that field, or
     * null if there is no piece in that field:
     *
     * <pre>{@code
     * List<Integer> values = new ArrayList<>();
     * for(int i = 0; i < this.getFields().size(); i++) {
     *     values.add(this.getField(i).value);
     * }
     * }</pre>
     *
     * The Board class has a main method which prints out a board where only
     * the first field (index 0) is filled with a piece with value 5 and the colors red on the bottom,
     * green on the left and blue on the right. It uses the ArrayLists defined on the top of the file.
     *
     * @param values The values of all fields on the board. This should be a List of exactly 36 items.
     *               If the field has no piece on it, the value is 'null', and if it does have a piece
     *               on it, it is the integer value of the piece.
     * @param vertical The letters of the vertical colors of all fields on the board. This should be a List of exactly
     *                 36 items. If the field has no piece on it, the value is 'null', and if it does have a piece
     *                 on it, the value is a character representing the color of the top or bottom side of the piece.
     * @param left The letters of the vertical colors of all fields on the board. This should be a List of exactly
     *             36 items. If the field has no piece on it, the value is 'null', and if it does have a piece
     *             on it, the value is a character representing the color of the left side of the piece.
     * @param right The letters of the vertical colors of all fields on the board. This should be a List of exactly
     *              36 items. If the field has no piece on it, the value is 'null', and if it does have a piece
     *              on it, the value is a character representing the color of the right side of the piece.
     * @return A string representing the state of the board as given.
     */
    public String getBoardString(){
        // All lists should have exactly 36 items.
        if(!Stream.of(values, vertical, left, right).parallel().map(List::size).allMatch(n -> n == 36)){
            throw new IllegalArgumentException("Input lists should all have 36 items, one for each field on the board.");
        }
        String template = "\n" +
                "                               ʌ                                                              ʌ\n" +
                "                              / \\                                                            / \\\n" +
                "                             / {f0b} \\                                                          /   \\\n" +
                "                            /{f00}   {f01}\\                                                        /     \\\n" +
                "                           /   {f02}   \\                                                      /{f0v}\\\n" +
                "                          /---------\\                                                    /---------\\\n" +
                "                         / \\   {f22}   / \\                                                  / \\{f2v}/ \\\n" +
                "                        / {f1b} \\{f20}   {f21}/ {f3b} \\                                                /   \\     /   \\\n" +
                "                       /{f10}   {f11}\\ {f2b} /{f30}   {f31}\\                                              /     \\   /     \\\n" +
                "                      /   {f12}   \\ /   {f32}   \\                                            /{f1v}\\ /{f3v}\\\n" +
                "                     /---------Ӿ---------\\                                          /---------Ӿ---------\\\n" +
                "                    / \\   {f52}   / \\   {f72}   / \\                                        / \\{f5v}/ \\{f7v}/ \\\n" +
                "                   / {f4b} \\{f50}   {f51}/ {f6b} \\{f70}   {f71}/ {f8b} \\                                      /   \\     /   \\     /   \\\n" +
                "                  /{f40}   {f41}\\ {f5b} /{f60}   {f61}\\ {f7b} /{f80}   {f81}\\                                    /     \\   /     \\   /     \\\n" +
                "                 /   {f42}   \\ /   {f62}   \\ /   {f82}   \\                                  /{f4v}\\ /{f6v}\\ /{f8v}\\\n" +
                "                /---------Ӿ---------Ӿ---------\\                                /---------Ӿ---------Ӿ---------\\\n" +
                "               / \\   {f102}   / \\   {f122}   / \\   {f142}   / \\                              / \\{f10v}/ \\{f12v}/ \\{f14v}/ \\\n" +
                "              / {f9b} \\{f100}   {f101}/ {f11b} \\{f120}   {f121}/ {f13b} \\{f140}   {f141}/ {f15b} \\                            /   \\     /   \\     /   \\     /   \\\n" +
                "             /{f90}   {f91}\\ {f10b} /{f110}   {f111}\\ {f12b} /{f130}   {f131}\\ {f14b} /{f150}   {f151}\\                          /     \\   /     \\   /     \\   /     \\\n" +
                "            /   {f92}   \\ /   {f112}   \\ /   {f132}   \\ /   {f152}   \\                        /{f9v}\\ /{f11v}\\ /{f13v}\\ /{f15v}\\\n" +
                "           /---------Ӿ---------Ӿ---------Ӿ---------\\                      /---------Ӿ---------Ӿ---------Ӿ---------\\\n" +
                "          / \\   {f172}   / \\   {f192}   / \\   {f212}   / \\   {f232}   / \\                    / \\{f17v}/ \\{f19v}/ \\{f21v}/ \\{f23v}/ \\\n" +
                "         / {f16b} \\{f170}   {f171}/ {f18b} \\{f190}   {f191}/ {f20b} \\{f210}   {f211}/ {f22b} \\{f230}   {f231}/ {f24b} \\                  /   \\     /   \\     /   \\     /   \\     /   \\\n" +
                "        /{f160}   {f161}\\ {f17b} /{f180}   {f181}\\ {f19b} /{f200}   {f201}\\ {f21b} /{f220}   {f221}\\ {f23b} /{f240}   {f241}\\                /     \\   /     \\   /     \\   /     \\   /     \\\n" +
                "       /   {f162}   \\ /   {f182}   \\ /   {f202}   \\ /   {f222}   \\ /   {f242}   \\              /{f16v}\\ /{f18v}\\ /{f20v}\\ /{f22v}\\ /{f24v}\\\n" +
                "      /---------Ӿ---------Ӿ---------Ӿ---------Ӿ---------\\            /---------Ӿ---------Ӿ---------Ӿ---------Ӿ---------\\\n" +
                "     / \\   {f262}   / \\   {f282}   / \\   {f302}   / \\   {f322}   / \\   {f342}   / \\          / \\{f26v}/ \\{f28v}/ \\{f30v}/ \\{f32v}/ \\{f34v}/ \\\n" +
                "    / {f25b} \\{f260}   {f261}/ {f27b} \\{f280}   {f281}/ {f29b} \\{f300}   {f301}/ {f31b} \\{f320}   {f321}/ {f33b} \\{f340}   {f341}/ {f35b} \\        /   \\     /   \\     /   \\     /   \\     /   \\     /   \\\n" +
                "   /{f250}   {f251}\\ {f26b} /{f270}   {f271}\\ {f28b} /{f290}   {f291}\\ {f30b} /{f310}   {f311}\\ {f32b} /{f330}   {f331}\\ {f34b} /{f350}   {f351}\\      /     \\   /     \\   /     \\   /     \\   /     \\   /     \\\n" +
                "  /   {f252}   \\ /   {f272}   \\ /   {f292}   \\ /   {f312}   \\ /   {f332}   \\ /   {f352}   \\    /{f25v}\\ /{f27v}\\ /{f29v}\\ /{f31v}\\ /{f33v}\\ /{f35v}\\\n" +
                " /-----------------------------------------------------------\\  /-----------------------------------------------------------\\\n";

        // Fill in bonus values
        template = listToMap(bonuses).entrySet().stream().reduce(template, (prev, elem) -> prev.replace("{f" + elem.getKey() + "b}", elem.getValue() != 1 ? String.valueOf(elem.getValue()) : " "), (s, s2) -> s);

        // Fill in values
        template = listToMap(values).entrySet().stream().reduce(template, (prev, elem) -> prev.replace("{f" + elem.getKey() + "v}", elem.getValue() != null ? String.format("(%d ,%2d)", (Math.round((Math.sqrt(elem.getValue()+0.25)-0.5))) + 1, (elem.getValue() - (Math.round((Math.sqrt(elem.getValue()+0.25)-0.5)))*(Math.round((Math.sqrt(elem.getValue()+0.25)-0.5))) - (Math.round((Math.sqrt(elem.getValue()+0.25)-0.5)))) ) : "  "), (s, s2) -> s);

        // Fill in left colors
        template = listToMap(left).entrySet().stream().reduce(template, (prev, elem) -> prev.replace("{f" + elem.getKey() + "0}", elem.getValue() != null ? String.valueOf(elem.getValue()) : " "), (s, s2) -> s);

        // Fill in right colors
        template = listToMap(right).entrySet().stream().reduce(template, (prev, elem) -> prev.replace("{f" + elem.getKey() + "1}", elem.getValue() != null ? String.valueOf(elem.getValue()) : " "), (s, s2) -> s);

        // Fill in vertical colors
        template = listToMap(vertical).entrySet().stream().reduce(template, (prev, elem) -> prev.replace("{f" + elem.getKey() + "2}", elem.getValue() != null ? String.valueOf(elem.getValue()) : " "), (s, s2) -> s);

        return template;
    }

    /**
     * Use to get the map.
     * 
     * @return datas, the map between the set tiles and their indexes.
     */
    /*
     * @ pure
     * 
     * @ ensures \result != null;
     */
    public Map<Integer, Tile> getBoard() {
        return datas;
    }
    
    /**
     * Use to attribute an unique index to a list of values.
     * 
     * @param inputList the list with given values
     * @return the map created with the specified list of values and their unique
     *         indexes
     */
    /*
     * @ requires inputList != null;
     * 
     * @ ensures \result.size() == imputList.size();
     * 
     * @ ensures (\forall i; 0 <= i && i < \result.size(); \result.get(i) ==
     * inputList.get(i));
     */
    private static <K> Map<Integer, K> listToMap(List<K> inputList){
        Map<Integer, K> indexed_values = new HashMap<>();
        for(int i = 0; i < values.size(); i++){ indexed_values.put(i, inputList.get(i)); }
        return indexed_values;
    }
    
    /**
     * Use to clear the board. IMPORTANT: The lists of neighbours are also emptied.
     */
    public void recovery() {
        for(Iterator<Entry<Integer, Tile>> it = datas.entrySet().iterator();it.hasNext();) {
            it.next();
            it.remove();
        }
        vertical =    Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        left =        Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        right =       Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
}
