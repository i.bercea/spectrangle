package com.project;

public class Tile {
	
    /**
     * The color of the left side of the tile.
     */
	private Character left = null;
	
	/**
     * The color of the right side of the tile.
     */
	private Character right = null;
	
	/**
     * The color of the top/bottom side of the tile.
     */
	private Character vertical = null;
	
	/**
     * The points the tile is worth.
     */
	private int values = 0;
	
	/**
     * The aspect of the tile (facing upwards or downwards).
     */
	private int aspect = -1;
	
	/**
     * The permutation of the tile.
     */
	private int state = -1;
	
	/*
     * @ requires input.size() == 5;
     * 
     * @ ensures left != null && right != null && vertical != null && (values >= 1
     * && values <= 6) && (state >= 0 && state <= 5) && (aspect == Aspect.upwards ||
     * aspect == Aspect.downwards);
     */
    /**
     * Tile constructor (used for communication through the protocol).
     * 
     * @param input A string that gives the aspect, value and colors of the sides of
     *              a tile.
     */
	public Tile(String input) {
		char[] encode = input.toCharArray();
		this.values = (int)encode[4] - (int)'0';
		this.state = (int)encode[0] - (int)'0';
		if(state==0) {
			this.left = encode[3];
			this.right = encode[1];
			this.vertical = encode[2];
			this.aspect = Aspect.upwards;
		}
		else if(state==1) {
			this.left = encode[2];
			this.right = encode[1];
			this.vertical = encode[3];
			this.aspect = Aspect.downwards;
		}
		else if(state==2) {
			this.left = encode[2];
			this.right = encode[3];
			this.vertical = encode[1];
			this.aspect = Aspect.upwards;
		}
		else if(state==3) {
			this.left = encode[1];
			this.right = encode[3];
			this.vertical = encode[2];
			this.aspect = Aspect.downwards;
		}
		else if(state==4) {
			this.left = encode[1];
			this.right = encode[2];
			this.vertical = encode[3];
			this.aspect = Aspect.upwards;
		}
		else if(state==5) {
			this.left = encode[3];
			this.right = encode[2];
			this.vertical = encode[1];
			this.aspect = Aspect.downwards;
		}
	}	
	
	@Override
	/**
     * @param obj An object to be compared with this Tile
     * @return true if the given object is equal to this Tile, otherwise false
     */
    /*
     * @ pure
     * 
     * @ requires obj != null;
     * 
     * @ ensures \result == true || \result == false;
     */
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		Tile sTile = (Tile)obj;
		if(
				(sTile.getLeft().equals(left)&&sTile.getRight().equals(right)&&sTile.getVertical().equals(vertical)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(left)&&sTile.getRight().equals(vertical)&&sTile.getVertical().equals(right)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(right)&&sTile.getRight().equals(left)&&sTile.getVertical().equals(vertical)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(right)&&sTile.getRight().equals(vertical)&&sTile.getVertical().equals(left)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(vertical)&&sTile.getRight().equals(right)&&sTile.getVertical().equals(left)&&sTile.getValues()==values)||
				(sTile.getLeft().equals(vertical)&&sTile.getRight().equals(left)&&sTile.getVertical().equals(right)&&sTile.getValues()==values))
			return true;
		else
			return false;
	}
	
	/**
     * Use to get the left color of this tile.
     * 
     * @return the color of the left side of this tile.
     */
    /*
     * @ pure
     * 
     * @ ensures \result == 'G' || \result == 'P' || \result == 'B' || \result ==
     * 'Y' || \result == 'R' || \resilt == 'W';
     */
	public Character getLeft() {
		return left;
	}
	
	/**
     * Use to set the color of the left side of this tile to a new one.
     * 
     * @param left A color to be set as the one of the left side of this tile.
     */
    /*
     * @ requires left == 'G' || left = 'P' || left == 'R' || left == 'B' || left ==
     * 'Y' || left == 'W';
     * 
     * @ ensures left == this.left;
     */
	public void setLeft(Character left) {
		this.left = left;
	}
	
	/**
     * Use to get the right color of this tile.
     * 
     * @return the color of the right side of this tile.
     */
    /*
     * @ pure
     * 
     * @ ensures \result == 'G' || \result == 'P' || \result == 'B' || \result ==
     * 'Y' || \result == 'R' || \result == 'W';
     */
	public Character getRight() {
		return right;
	}
	
	/**
     * Use to set the color of the right side of this tile to a new one.
     * 
     * @param right A color to be set as the one of the right side of this tile.
     */
    /*
     * @ requires right == 'G' || right = 'P' || right == 'R' || right == 'B' ||
     * right == 'Y' || right == 'W';
     * 
     * @ ensures this.right = right;
     */
	public void setRight(Character right) {
		this.right = right;
	}
	
	/**
     * Use to get the vertical color of this tile.
     * 
     * @return the color of the vertical side of this tile.
     */
    /*
     * @ pure
     * 
     * @ ensures \result == 'G' || \result == 'P' || \result == 'B' || \result ==
     * 'Y' || \result == 'R' || \result == 'W';
     */
	public Character getVertical() {
		return vertical;
	}
	
	/**
     * Use to set the color of the vertical side of this tile to a new one.
     * 
     * @param vertical A color to be set as the one of the vertical side of this
     *                 tile.
     */
    /*
     * @ requires vertical == 'G' || vertical = 'P' || vertical == 'R' || vertical
     * == 'B' || vertical == 'Y' || vertical == 'W';
     * 
     * @ ensures this.vertical = vertical;
     */
	public void setVertical(Character vertical) {
		this.vertical = vertical;
	}
	
	/**
     * Use to get the points this tile is worth.
     * 
     * @return An int representing the number of points this tile is worth.
     */
    /*
     * @ pure
     * 
     * @ ensures \result >= 1 && \result <= 6;
     */
	public int getValues() {
		return values;
	}
	
	/**
     * Use to set the value of this tile (the number of points it is worth).
     * 
     * @param values An int representing the number of points this tile should be
     *               worth.
     */
    /*
     * @ requires values >= 1 && values <= 6;
     * 
     * @ ensures this.values == values;
     */
	public void setValues(int values) {
		this.values = values;
	}
	
	/**
     * Use to get the aspect of this tile (upwards or downwards).
     * 
     * @return The aspect of this tile.
     */
    /*
     * @ pure
     * 
     * @ ensures \result == Aspect.upwards || \result == Aspect.downwards;
     */
	public int getAspect() {
		return aspect;
	}

	/**
     * Use to set a Tile
     * 
     * @param input The encoding of a tile
     */
    /*
     * @ requires input != null && input.size() == 5;
     * 
     * @ ensures left != null && right != null && vertical != null && (values >= 1
     * && values <= 6) && (state >= 0 && state <= 5) && (aspect == Aspect.upwards ||
     * aspect == Aspect.downwards);
     */
	private void setTile(String input) {
		char[] encode = input.toCharArray();
		this.values = (int)encode[4] - (int)'0';
		this.state = (int)encode[0] - (int)'0';
		if(state==0) {
			this.left = encode[3];
			this.right = encode[1];
			this.vertical = encode[2];
			this.aspect = Aspect.upwards;
		}
		else if(state==1) {
			this.left = encode[2];
			this.right = encode[1];
			this.vertical = encode[3];
			this.aspect = Aspect.downwards;
		}
		else if(state==2) {
			this.left = encode[2];
			this.right = encode[3];
			this.vertical = encode[1];
			this.aspect = Aspect.upwards;
		}
		else if(state==3) {
			this.left = encode[1];
			this.right = encode[3];
			this.vertical = encode[2];
			this.aspect = Aspect.downwards;
		}
		else if(state==4) {
			this.left = encode[1];
			this.right = encode[2];
			this.vertical = encode[3];
			this.aspect = Aspect.upwards;
		}
		else if(state==5) {
			this.left = encode[3];
			this.right = encode[2];
			this.vertical = encode[1];
			this.aspect = Aspect.downwards;
		}
	}
	
	/**
     * Use to set the aspect of this field.
     * 
     * @param place An array of ints: place[0] the row, place[1] the column
     */
    /*
     * @ requires (place[0] >= 0 && place[0] <= 5) && (place[1] >= -5 && place[1] <=
     * 5);
     * 
     * @ ensures aspect == Aspect.upwards || Aspect.downwards;
     */
	public void setAspect(int[] place) {
		int r = place[0];
		int c = place[1];
		String temp = toEncoding();		
		if((r+c)%2==0) {
			this.aspect = Aspect.upwards;
			if(state==1||state==3||state==5) {
				state = (state+1)%6;
				setTile(state+temp.substring(1, temp.length()));
			}
		}
		else {
			this.aspect = Aspect.downwards;
			if(state==0||state==2||state==4) {
				state = (state+1)%6;
				setTile(state+temp.substring(1, temp.length()));
			}
		}
	}
	
	/**
    * Use to encode a Tile.
    * 
    * @return A string representing the tile encryption.
    */
   /*
    * @ pure
    * 
    * @ requires left != null && right != null && vertical != null && (values >= 1
    * && values <= 6) && (state >= 0 && state <= 5) && (aspect == Aspect.upwards ||
    * aspect == Aspect.downwards);
    * 
    * @ ensures \result.size() == 5;
    */
	public String toEncoding() {
		if(state==0)
			return String.format("%d%c%c%c%d", state, right, vertical, left, values);
		else if(state==1)
			return String.format("%d%c%c%c%d", state, right, left, vertical, values);
		else if(state==2)
			return String.format("%d%c%c%c%d", state, vertical, left, right, values);
		else if(state==3)
			return String.format("%d%c%c%c%d", state, left, vertical, right, values);
		else if(state==4)
			return String.format("%d%c%c%c%d", state, left, right, vertical, values);
		else if(state==5)
			return String.format("%d%c%c%c%d", state, vertical, right, left, values);
		return null;
	}
	
	/**
     * Use to turn this tile around clockwise.
     * 
     * @param angle The current permutation of this tile (0, 120 or 240 degrees).
     * @return The permuted tile if the permutation exists (i.e. 0, 120 or 240
     *         degrees), otherwise null
     */
    /*
     * @ requires angle == Angle.turn_0 || angle == Angle.turn_120 || angle ==
     * Angle.turn_240;
     */
	public Tile turn(int angle) {
		if(angle == Angle.turn_0)
			return this;
		else if(angle == Angle.turn_120) {
			if(state==0)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, right, vertical, left, values));
			else if(state==1)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, right, left, vertical, values));
			else if(state==2)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, vertical, left, right, values));
			else if(state==3)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, left, vertical, right, values));
			else if(state==4)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, left, right, vertical, values));
			else if(state==5)
				return new Tile(String.format("%d%c%c%c%d", (state+2)%6, vertical, right, left, values));
		}
		else if(angle == Angle.turn_240){
			if(state==0)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, right, vertical, left, values));
			else if(state==1)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, right, left, vertical, values));
			else if(state==2)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, vertical, left, right, values));
			else if(state==3)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, left, vertical, right, values));
			else if(state==4)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, left, right, vertical, values));
			else if(state==5)
				return new Tile(String.format("%d%c%c%c%d", (state+4)%6, vertical, right, left, values));
		}
		return null;
	}
	
}
