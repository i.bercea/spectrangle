package com.project;

public class Difficulty {
    
    /**
     * Random difficulty
     */
	public static final int random = 0;
	
	/**
     * Smart difficulty
     */
	public static final int smart = 1;
}
