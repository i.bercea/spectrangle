package com.network;

import java.util.List;
import java.util.Map;

import com.model.Tile;

public interface Transmit {
	public void extensionSign(String username, String hostname, boolean chat, boolean challenge, boolean leaderboard, boolean security);
	public void play(String username, int amount);
	public void move(String username, int index, String tileEncoding);
	public void close(String hostname);
	public void order(List<String> userList);
	public void give(List<String> userList, String username, Tile[] tiles);
	public void turn(List<String> userList, String username);
	public void end(Map<String, Integer> userList);
}
