package com.network;

public class ExtensionSign {
	private String username = null;
	private boolean chat = false;
	private boolean challenge = false;
	private boolean leaderboard = false;
	private boolean security = false;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("<%s><%b><%b><%b><%b>", username,chat,challenge,leaderboard,security);
	}

	public ExtensionSign(String username, boolean chat, boolean challenge, boolean leaderboard, boolean security) {
		this.username = username;
		this.chat = chat;
		this.challenge = challenge;
		this.leaderboard = leaderboard;
		this.security = security;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isChat() {
		return chat;
	}
	public void setChat(boolean chat) {
		this.chat = chat;
	}
	public boolean isChallenge() {
		return challenge;
	}
	public void setChallenge(boolean challenge) {
		this.challenge = challenge;
	}
	public boolean isLeaderboard() {
		return leaderboard;
	}
	public void setLeaderboard(boolean leaderboard) {
		this.leaderboard = leaderboard;
	}
	public boolean isSecurity() {
		return security;
	}
	public void setSecurity(boolean security) {
		this.security = security;
	}
	
}
