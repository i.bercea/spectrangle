package com.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.model.Angle;
import com.model.Field;
import com.model.Tile;
import com.project.Board;
import com.project.HumanPlayer;

public class Client extends Thread{
	
    /**
     * The server port.
     */
	private static final int port = 1024;
	
	/**
	 * The server socket.
	 */
	private Socket socket = null;
	
	/**
	 * The input stream.
	 */
	private InputStream inputStream = null;
	
	/**
	 * The output stream.
	 */
	private OutputStream outputStream = null;
	
	/**
	 * String representation of the hand of a player.
	 */
	private Field field = null;
	
	/**
	 * The spectrangle board.
	 */
	private Board board = null;
	
	/**
	 * Means of input of the client.
	 */
	private Scanner scanner = null;
	
	/**
	 * Client Constructor.
	 * @param hostname The IP of the server the client connects to
	 * @param field The string representation of the hand of tiles of a player.
	 * @param board The spectrangle board.
	 * @param scanner The means of input of the client (from the console)
	 */
	/*
	 * @ requires hostname != null && field != null, board != null && scanner != null;
	 * @ ensures this.hostname == hostname && this.field == field && this.board == board && this.scanner == scanner;
	 */
	public Client(String hostname, Field field, Board board, Scanner scanner) {
		// TODO Auto-generated constructor stub
		try {
			System.out.println("client:hostname is "+hostname);
			this.field = field;
			this.board = board;
			this.scanner = scanner;
			socket = new Socket(hostname, port);
			inputStream = socket.getInputStream();
			outputStream = socket.getOutputStream();
			System.out.println("client:socket.InetAddress.hostname is "+socket.getInetAddress().getHostAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Client.UnKnownHostError:"+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Client.IOError:"+e.getMessage());
		}
	}
	
	/**
	 * Use to read a line from the input.
	 * @return if the server is open and the connection is not a test one, the next line from the input stream, "closed" if
	 *         the server is closed, otherwise null
	 */
	/*
	 * @ requires socket.isOpen();
	 */
	public String read() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String data = bufferedReader.readLine();
			if(data!=null&&data.equals("TestConnection"))
				return null;
			else
				return data;
		} catch (IOException e) {
			// TODO: handle exception
			if(e.getMessage().indexOf("Connection reset")!=-1) {
				System.out.println("Server is closed, please try restarting the client.");
				close();
				return "closed";
			}
			else if(e.getMessage().indexOf("closed")!=-1) {
				System.out.println("Server is closed, please try restarting the client.");
				close();
				return "closed";
			}
			else
				System.out.println("Client.IOError:"+e.getMessage());
			return null;
		}
	}
	
	@Override
	/**
	 * Use to run the client.
	 */
	/*
	 * @ requires scanner != null;
	 */
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		System.out.print("\nPlease enter your nickname:");
		String username = null;
		while(true) {
			username = scanner.next();
			if(username.matches("^(\\w+)$"))
				break;
			else
				System.out.print("The nickname format is incorrect. Please re-enter your nickname:");
		}
		System.out.print("\nDo you need the chat function (please enter yes or no):");
		boolean chat = false;
		while(true) {
			String temp = scanner.next();
			if(temp.matches("^(?:yes|no)$")) {
				chat = temp.equals("yes")?true:false;
				break;
			}
			else
				System.out.print("\nplease enter yes or no:");
		}
		System.out.print("\nDo you need the challenge function (please enter yes or no):");
		boolean challenge = false;
		while(true) {
			String temp = scanner.next();
			if(temp.matches("^(?:yes|no)$")) {
				challenge = temp.equals("yes")?true:false;
				break;
			}
			else
				System.out.print("\nplease enter yes or no:");
		}
		System.out.print("\nDo you need the leaderboard function (please enter yes or no):");
		boolean leaderboard = false;
		while(true) {
			String temp = scanner.next();
			if(temp.matches("^(?:yes|no)$")) {
				leaderboard = temp.equals("yes")?true:false;
				break;
			}
			else
				System.out.print("\nplease enter yes or no:");
		}
		System.out.print("\nDo you need the security function (please enter yes or no):");
		boolean security = false;
		while(true) {
			String temp = scanner.next();
			if(temp.matches("^(?:yes|no)$")) {
				security = temp.equals("yes")?true:false;
				break;
			}
			else
				System.out.print("\nplease enter yes or no:");
		}
		write(String.format("Hello<%s><%b><%b><%b><%b>", username,chat,challenge,leaderboard,security));
		System.out.print("\nHow many people do you want to play games with?(please enter 1 to 3):");
		int amount = 0;
		while(true) {	
			try {
				amount = new Integer(scanner.next()) + 1;
				if(amount==2|amount==3|amount==4)
					break;
				else {
					System.out.print("\nThe number entered is not in the list!" +
							"\nplease enter 1 to 3:");
					continue;
				}
			} catch (NumberFormatException e) {
				// TODO: handle exception
				System.out.println("\nThe input is not a number!");
				continue;
			}
		}
		write(String.format("Play<%d>", amount));
		String readData = null;
		while(true) {
			while((readData = read())==null||!(readData.matches(Match.waiting))) {
				if(readData!=null&&readData.equals("closed")) {
					System.out.println("Server closed.");
					System.exit(0);
				}
			};
			if(readData.equals("Waiting<0>")) {
				readData = null;
				break;
			}
		}
		System.out.println("The Game is start");
		
		List<HumanPlayer> sequence_list = new ArrayList<>();
		switch (amount) {
		case 2:
			sequence_list.add(new HumanPlayer());
			sequence_list.add(new HumanPlayer());
			break;
		case 3:
			sequence_list.add(new HumanPlayer());
			sequence_list.add(new HumanPlayer());
			sequence_list.add(new HumanPlayer());
			break;
		case 4:
			sequence_list.add(new HumanPlayer());
			sequence_list.add(new HumanPlayer());
			sequence_list.add(new HumanPlayer());
			sequence_list.add(new HumanPlayer());
			break;
		default:
			break;
		}
		
		while(true) {
			while((readData = read())==null||!(readData.matches(Match.order))) {
				if(readData!=null&&readData.equals("closed")) {
					System.out.println("Server closed.");
					System.exit(0);
				}
			}
			if(readData.matches("Order<\\w+><\\w+>")){
				System.out.print("The order of the game is:");
				Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
				Matcher matcher = intercept.matcher(readData);
				for(int i=0;matcher.find();i++) {
					String pickname = matcher.group();
					sequence_list.get(i).setUsername(pickname);
					System.out.print(pickname+". ");
				}
				System.out.print("\n");
				break;
			}else if(readData.matches("Order<\\w+><\\w+>[\\w+]")) {
				System.out.print("The order of the game is:");
				Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
				Matcher matcher = intercept.matcher(readData);
				for(int i=0;matcher.find();i++) {
					String pickname = matcher.group();
					sequence_list.get(i).setUsername(pickname);
					System.out.print(pickname+". ");
				}
				intercept = Pattern.compile("(?<=\\[).*?(?=\\])");
				matcher = intercept.matcher(readData);
				for(int i=2;matcher.find();i++) {
					String pickname = matcher.group();
					sequence_list.get(i).setUsername(pickname);
					System.out.print(pickname+". ");
				}
				System.out.print("\n");
				break;
			}else if(readData.matches("Order<\\w+><\\w+>[\\w+][\\w+]")) {
				System.out.print("The order of the game is:");
				Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
				Matcher matcher = intercept.matcher(readData);
				for(int i=0;matcher.find();i++) {
					String pickname = matcher.group();
					sequence_list.get(i).setUsername(pickname);
					System.out.print(pickname+". ");
				}
				intercept = Pattern.compile("(?<=\\[).*?(?=\\])");
				matcher = intercept.matcher(readData);
				for(int i=2;matcher.find();i++) {
					String pickname = matcher.group();
					sequence_list.get(i).setUsername(pickname);
					System.out.print(pickname+". ");
				}
				System.out.print("\n");
				break;
			}
		}
		
		for(int j=0;j<amount;) {
			while((readData = read())==null||!(readData.matches(Match.give))) {
				if(readData!=null&&readData.equals("closed")) {
					System.out.println("Server closed.");
					System.exit(0);
				}
			}
			if(readData.indexOf("Give")!=-1){
				j++;
				Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
				Matcher matcher = intercept.matcher(readData);
				String pickname =null;
				Tile tile = null;
				HumanPlayer humanPlayer = null;
				for(int i=0;matcher.find();i++) {
					if(i==0)
						 pickname = matcher.group();
					else
						tile = new Tile(matcher.group());
				}
				for(int i=0;i<sequence_list.size();i++)
					if(sequence_list.get(i).getUsername().equals(pickname))
						humanPlayer = sequence_list.get(i);
				humanPlayer.addTile(tile);
				intercept = Pattern.compile("(?<=\\[).*?(?=\\])");
				matcher = intercept.matcher(readData);
				while(matcher.find())
					humanPlayer.addTile(new Tile(matcher.group()));
				break;
			}
		}
		
		while(true) {
			readData = read();
			if(readData!=null&&readData.equals("closed")) {
				System.out.println("Server closed.");
				System.exit(0);
			}
			if(readData!=null&&readData.matches(Match.end)) {
				if(readData.matches(Match.end2)) {
					System.out.printf(
							"\nThe score of %s is %s.\n", 
							readData.substring(readData.indexOf("<<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">>")));
					readData = readData.substring(readData.indexOf(">>")+2);
					System.out.printf(
							"The score of %s is %s.\n", 
							readData.substring(readData.indexOf("<<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">>")));
				}else if(readData.matches(Match.end3)) {
					System.out.printf(
							"\nThe score of %s is %s.\n", 
							readData.substring(readData.indexOf("<<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">>")));
					readData = readData.substring(readData.indexOf(">>")+2);
					System.out.printf(
							"The score of %s is %s.\n", 
							readData.substring(readData.indexOf("<<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">>")));
					readData = readData.substring(readData.indexOf(">]")+2);
					System.out.printf(
							"The score of %s is %s.\n", 
							readData.substring(readData.indexOf("[<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">]")));
				}else if(readData.matches(Match.end4)) {
					System.out.printf(
							"\nThe score of %s is %s.\n", 
							readData.substring(readData.indexOf("<<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">>")));
					readData = readData.substring(readData.indexOf(">>")+2);
					System.out.printf(
							"The score of %s is %s.\n", 
							readData.substring(readData.indexOf("<<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">>")));
					readData = readData.substring(readData.indexOf(">]")+2);
					System.out.printf(
							"The score of %s is %s.\n", 
							readData.substring(readData.indexOf("[<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">]")));
					readData = readData.substring(readData.indexOf(">]")+2);
					System.out.printf(
							"The score of %s is %s.\n", 
							readData.substring(readData.indexOf("[<")+2, readData.indexOf(">,")),
							readData.substring(readData.indexOf(",<")+2, readData.indexOf(">]")));
				}
			}
			if(readData!=null&&readData.matches(Match.turn)) {
				if(readData.substring(readData.indexOf("<")+1, readData.indexOf(">")).equals(username)) {
					System.out.println(board.getBoardString());
					HumanPlayer I = null;
					for(HumanPlayer humanPlayer:sequence_list) {
						if(humanPlayer.getUsername().equals(username))
							I = humanPlayer;
						System.out.printf("\n%s's tiles(%d):\n", humanPlayer.getUsername(), humanPlayer.getScore());
						System.out.println(field.getFieldString(humanPlayer.geTiles(), true));
					}
					while(true) {
						try {
							System.out.println("\nYou can do the following:\n1.play chess\n2.skip.\n3.Exchange and skip.");
							int operation = new Integer(scanner.next());
							if(operation==1) {
								System.out.printf("Choose which tile to play [1..%d]:",I.geTiles().length);
								int tile_index = -1;
								int tile_row = -1;
								int tile_column = -1;
								while((tile_index = new Integer(scanner.next()) - 1)>=I.geTiles().length||tile_index<0)
									System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
								System.out.print("Choose which row to play [1..6]:");
								while((tile_row = new Integer(scanner.next()) - 1)>5||tile_row<0)
									System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
								System.out.printf("Choose which column to play: [−%d..%d]:",tile_row,tile_row);
								while((tile_column = new Integer(scanner.next()))>tile_row||tile_column<-tile_row)
									System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
								Tile[] tiles = new Tile[3];
								for(int i=0;i<3;i++)
						    		tiles[i] = I.geTiles()[tile_index].setAspect(new int[] {tile_row, tile_column}).turn(i);
								System.out.println(
										field.getTileString(tiles, tile_row, tile_column));
								System.out.printf(
										"Choose which permutation to play [1..%d]: ", tiles.length);
								int tile_angle = -1;
								while((tile_angle = new Integer(scanner.next()) - 1)>tiles.length-1||tile_angle<0)
									System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
								int[] place = new int[2];
								place[0] = tile_row;
								place[1] = tile_column;
								board.addTile(tiles[tile_angle], place, Angle.turn_0);
								write(String.format("Move<%d><%s>", tile_row*tile_row+tile_row+tile_column, tiles[tile_angle]));
							}else if(operation==2) {
								write("Skip");
							}else if(operation==3) {
								System.out.printf("Choose which tile to put back [1..%d]:",I.geTiles().length);
								int tile_index = -1;
								while(true) {
									tile_index = new Integer(scanner.next()) - 1;
									if(tile_index>=I.geTiles().length||tile_index<0)
										System.out.print("\nThe number entered is not in the list!\nPlease re-enter!");
									else
										break;
								}
								write(String.format("Skip[%s]", I.geTiles()[tile_index].toEncoding()));
							}else {
								System.out.print("\nThe number entered is not in the list!" +
										"\nPlease re-enter the operation number:"
										+ "\n1.play chess\n2.skip.\n3.Exchange and skip.");
							}
						} catch (NumberFormatException e) {
							// TODO: handle exception
							System.out.print("\nThe input is not a number!" +
									"\nPlease re-enter the operation number:\n1.play chess\n2.skip.\n3.Exchange and skip.");
							continue;
						}
					}
				}else{
					System.out.printf("\nNow it's %s playing games.", 
							readData.substring(readData.indexOf("<")+1, readData.indexOf(">")));
				}
			}
			if(readData!=null&&readData.matches(Match.move_cilent)) {
				Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
				Matcher matcher = intercept.matcher(readData);
				HumanPlayer hPlayer = null;
				String pickname = null;
				int index = -1;
				String tileEncoding = null;				
				for(int i=0;matcher.find();i++) {
					switch (i) {
					case 0:
						pickname = matcher.group();
						break;
					case 1:
						index = new Integer(matcher.group());
						break;
					case 2:
						tileEncoding = matcher.group();
						break;
					default:
						break;
					}
				}
				int r = (int) (Math.round((Math.sqrt(index+0.25)-0.5)));
				int c = index - r*r - r;
				int[] place = {r,c};
				for(HumanPlayer humanPlayer:sequence_list)
					if(humanPlayer.getUsername().equals(pickname))
						hPlayer = humanPlayer;
				hPlayer.removeTile(new Tile(tileEncoding));
				board.addTile(new Tile(tileEncoding), place, Angle.turn_0);
			}	
			if(readData!=null&&readData.matches(Match.give1)) {
				Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
				Matcher matcher = intercept.matcher(readData);
				String pickname =null;
				Tile tile = null;
				HumanPlayer humanPlayer = null;
				for(int i=0;matcher.find();i++) {
					if(i==0)
						 pickname = matcher.group();
					else
						tile = new Tile(matcher.group());
				}
				for(int i=0;i<sequence_list.size();i++)
					if(sequence_list.get(i).getUsername().equals(pickname))
						humanPlayer = sequence_list.get(i);
				humanPlayer.addTile(tile);
			}
			
		}
		
	}

	/**
	 * Use to write something to the output stream.
	 * @param data The string to be written to the output stream.
	 */
	/*
	 * @ requires data != null; 
	 * @ ensures !socket.isInputShutdown() && !socket.isOutputShutdown() && !socket.isClosed();
	 */
	public void write(String data) {
		try {
			BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
			bufferWriter.write(data+"\n");
			bufferWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getMessage().indexOf("Connection reset")!=-1) {
				System.out.println("Server is closed, please try restarting the client.");
				close();
			}
			else
				System.out.println("Client.IOError:"+e.getMessage());
		}
	}
	
	/**
	 * Use to close the connection.
	 * @return true if the connection was terminated, false otherwise
	 */
	/*
	 * @ requires socket != null;
	 * @ ensures socket.isClosed() && socket.isInputShutdown() && socket.isOutputShutdown();
	 */
	public boolean close() {
		try {
			if(socket.isInputShutdown()&&socket.isOutputShutdown()&&socket.isClosed())
				return true;
			else {
				inputStream.close();
				outputStream.close();
				//socket.shutdownInput();
				//socket.shutdownOutput();
				socket.close();
				return socket.isInputShutdown()&&socket.isOutputShutdown()&&socket.isClosed();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getMessage().indexOf("closed")!=-1) {
				System.out.println("Server is closed, please try restarting the client.");
				return true;
			}
			System.out.println("Client.error:"+e.getMessage());
			return socket.isInputShutdown()&&socket.isOutputShutdown()&&socket.isClosed();
		}
	}
	
}
