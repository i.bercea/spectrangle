package com.network;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import com.model.Tile;

public class Server implements Transmit{
	private static final int port = 1024;
	private ServerSocket serverSocket = null;
	private Map<String, ServerThread> serverThreads = new HashMap<>();
	private Map<String, ExtensionSign> extensions = new HashMap<>();
	private Map<String, Integer> waitingRoom = new HashMap<>();
	private Map<String, String> mapping = new HashMap<>();
	private Map<String, Queue<MoveData>> moveQueue = new HashMap<>();
	private Map<String, GameThread> gameThreads = new HashMap<>();
	
	public Server() {
		// TODO Auto-generated constructor stub
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("server is running");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Server.error:"+e.getMessage());
		}
		
	}
	
	public void run() {
		
		try {
			
			while (true) {
				System.out.println("Monitor is ready");
	            Socket socket = serverSocket.accept();
	            System.out.println("Monitor is running");
	            if(serverThreads.containsKey(socket.getInetAddress().getHostAddress())) {
	            	BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	            	bufferedWriter.write("Error<YouAlsoRunOtherClient>\n");
	            	bufferedWriter.flush();
	            	socket.shutdownInput();
	    			socket.shutdownOutput();
	    			socket.close();
	            }
	            else {
		            ServerThread serverThread = new ServerThread(this,socket);
		            serverThread.start();
		            serverThreads.put(serverThread.getHostname(),serverThread);
		            System.out.println("ServerThreads.size is "+serverThreads.size());
	            }
	        }
			
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("Server.error:"+e.getMessage());
		}
				
	}
	
	@Override
	public void close(String hostname) {
		serverThreads.get(hostname).close();
		serverThreads.remove(hostname);
		for(String username:mapping.keySet()) {
			if(mapping.get(username).equals(hostname)) {
				mapping.remove(username);
			}
		}
	}

	@Override
	public void extensionSign(String username, String hostname, boolean chat, boolean challenge, boolean leaderboard, boolean security) {
		// TODO Auto-generated method stub
		extensions.put(username, new ExtensionSign(username, chat, challenge, leaderboard, security));
		mapping.put(username, hostname);
	}

	@Override
	public void play(String username, int amount) {
		// TODO Auto-generated method stub
		waitingRoom.put(username, amount);
		List<String> twoWaitingRoom = new ArrayList<>();
		List<String> thirdWaitingRoom = new ArrayList<>();
		List<String> fourWaitingRoom = new ArrayList<>();
		for(String key:waitingRoom.keySet()) {
			switch (waitingRoom.get(key)) {
			case 2:	
				twoWaitingRoom.add(key);
				break;
			case 3:		
				thirdWaitingRoom.add(key);
				break;
			case 4:		
				fourWaitingRoom.add(key);
				break;
			default:
				break;
			}
		}
		if(twoWaitingRoom.size()==2) {
			GameThread gameThread = new GameThread(this, twoWaitingRoom.get(0), twoWaitingRoom.get(1));
			gameThread.start();
			for(String user:twoWaitingRoom) {
				serverThreads.get(mapping.get(user)).write("Waiting<0>");
				gameThreads.put(user, gameThread);
			}
		}else {
			for(String user:twoWaitingRoom)
				serverThreads.get(mapping.get(user)).write("Waiting<1>");
		}
		if(thirdWaitingRoom.size()==3) {
			GameThread gameThread = new GameThread(
					this, 
					thirdWaitingRoom.get(0), 
					thirdWaitingRoom.get(1), 
					thirdWaitingRoom.get(2));
			gameThread.start();
			for(String user:thirdWaitingRoom){
				serverThreads.get(mapping.get(user)).write("Waiting<0>");
				gameThreads.put(user, gameThread);
			}
		}else {
			for(String user:thirdWaitingRoom)
				serverThreads.get(mapping.get(user)).write(String.format("Waiting<%d>", 3-thirdWaitingRoom.size()));
		}
		if(fourWaitingRoom.size()==4) {
			GameThread gameThread = new GameThread(
					this, 
					fourWaitingRoom.get(0), 
					fourWaitingRoom.get(1), 
					fourWaitingRoom.get(2), 
					fourWaitingRoom.get(3));
			gameThread.start();
			for(String user:fourWaitingRoom){
				serverThreads.get(mapping.get(user)).write("Waiting<0>");
				gameThreads.put(user, gameThread);
			}
		}else {
			for(String user:fourWaitingRoom)
				serverThreads.get(mapping.get(user)).write(String.format("Waiting<%d>", 4-fourWaitingRoom.size()));
		}
		
	}

	@Override
	public void order(List<String> userList) {
		// TODO Auto-generated method stub
		System.out.println(userList);
		String data = "Order";
		for(int i=0;i<userList.size();i++) {
			if(i==0||i==1)
				data += String.format("<%s>", userList.get(i));
			if(i==2||i==3)
				data += String.format("[%s]", userList.get(i));
		}
		for(int i=0;i<userList.size();i++) {
			if(mapping.containsKey(userList.get(i))&&serverThreads.containsKey(mapping.get(userList.get(i)))) {
				System.out.println(serverThreads.get(mapping.get(userList.get(i))).getUsername() + " "+ data);
				serverThreads.get(mapping.get(userList.get(i))).write(data);
			}
		}
	}

	@Override
	public void give(List<String> userList, String username, Tile[] tiles) {
		// TODO Auto-generated method stub
		String data = String.format("Give<%s>", username);
		for(int i=0;i<tiles.length;i++) {
			if(i==0)
				data += String.format("<%s>", tiles[i].toEncoding());
			if(i==1|i==2|i==3)
				data += String.format("[%s]", tiles[i].toEncoding());
		}
		for(int i=0;i<userList.size();i++)
			if(mapping.containsKey(userList.get(i))&&serverThreads.containsKey(mapping.get(userList.get(i))))
				serverThreads.get(mapping.get(userList.get(i))).write(data);
	}

	@Override
	public void turn(List<String> userList, String username) {
		// TODO Auto-generated method stub
		String data = String.format("Turn<%s>", username);
		for(int i=0;i<userList.size();i++) {
			if(mapping.containsKey(userList.get(i))&&serverThreads.containsKey(mapping.get(userList.get(i)))) {
				serverThreads.get(mapping.get(userList.get(i))).write(data);
				System.out.println(serverThreads.get(mapping.get(userList.get(i))).getUsername() + " "+ data);
			}
		}
	}

	@Override
	public void end(Map<String, Integer> userList) {
		// TODO Auto-generated method stub
		String data = "End";
		int i = 0;
		for(String username:userList.keySet()) {
			if(i==0||i==1)
				data += String.format("<<%s>,<%d>>", username, userList.get(username));
			if(i==2||i==3)
				data += String.format("[<%s>,<%d>]", username, userList.get(username));
		}
		for(String username:userList.keySet()) {
			serverThreads.get(mapping.get(username)).write(data);
			gameThreads.remove(username);
		}
	}

	@Override
	public void move(String username, int index, String tileEncoding) {
		// TODO Auto-generated method stub
		if(moveQueue.containsKey(username)) {
			Queue<MoveData> queue = moveQueue.get(username);
			queue.offer(new MoveData(index, tileEncoding));
			moveQueue.put(username, queue);
			gameThreads.get(username).conversion();
		}else {
			Queue<MoveData> queue = new LinkedList<>();
			queue.offer(new MoveData(index, tileEncoding));
			moveQueue.put(username, queue);
			gameThreads.get(username).conversion();
		}
	}
	
	public MoveData getMoveData(List<String> userList, String username) {
		if(moveQueue.containsKey(username)) {
			Queue<MoveData> queue = moveQueue.get(username);
			MoveData moveData = queue.poll();
			for(String pickname:userList) {
				serverThreads.get(mapping.get(pickname)).write(
						String.format("Move<%s><%d><%s>", username, moveData.getIndex(), moveData.getTileEncoding()));
			}
			return moveData;
		}else
			return null;
	}
	
}
