package com.network;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.model.Angle;
import com.model.Tile;
import com.project.Bag;
import com.project.Board;
import com.project.HumanPlayer;

public class GameThread extends Thread {
	
	private static Board board = new Board();
	private static Bag bag = new Bag();
	private Server server = null;
	
	private static HumanPlayer player1 = new HumanPlayer();
	private static HumanPlayer player2 = new HumanPlayer();
	private static HumanPlayer player3 = null;
	private static HumanPlayer player4 = null;
	
	private boolean logo = true;
	
	public GameThread(Server server, String username1, String username2) {
		// TODO Auto-generated constructor stub
		this.server = server;
		player1.setUsername(username1);
		player2.setUsername(username2);
	}
	
	public GameThread(Server server, String username1, String username2, String username3) {
		// TODO Auto-generated constructor stub
		this.server = server;
		player1.setUsername(username1);
		player2.setUsername(username2);
		player3 = new HumanPlayer();
		player3.setUsername(username3);
	}
	
	public GameThread(Server server, String username1, String username2, String username3, String username4) {
		// TODO Auto-generated constructor stub
		this.server = server;
		player1.setUsername(username1);
		player2.setUsername(username2);
		player3 = new HumanPlayer();
		player4 = new HumanPlayer();
		player3.setUsername(username3);
		player4.setUsername(username4);
	}
	
	private static boolean endGameJudge(int amount) {
		switch (amount) {
		case 2:
			return (bag.getNumber()==0) && (
					((player1.isPlay(board.getBoard())==null)||(player1.isPlay(board.getBoard()).size()==0)) &&
					((player2.isPlay(board.getBoard())==null)||(player2.isPlay(board.getBoard()).size()==0))
					);
		case 3:
			return (bag.getNumber()==0) && (
					((player1.isPlay(board.getBoard())==null)||(player1.isPlay(board.getBoard()).size()==0)) &&
					((player2.isPlay(board.getBoard())==null)||(player2.isPlay(board.getBoard()).size()==0)) &&
					((player3.isPlay(board.getBoard())==null)||(player3.isPlay(board.getBoard()).size()==0)) 
					);
		case 4:
			return (bag.getNumber()==0) && (
					((player1.isPlay(board.getBoard())==null)||(player1.isPlay(board.getBoard()).size()==0)) &&
					((player2.isPlay(board.getBoard())==null)||(player2.isPlay(board.getBoard()).size()==0)) &&
					((player3.isPlay(board.getBoard())==null)||(player3.isPlay(board.getBoard()).size()==0)) &&
					((player4.isPlay(board.getBoard())==null)||(player4.isPlay(board.getBoard()).size()==0)) 
					);
		default:
			break;
		}
		return false;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		int amount = 0;
		List<HumanPlayer> sequence_list = new ArrayList<>();
		if(player3==null&&player4==null) {
			amount = 2;
			
			player1.setFirstCard(bag.getTile());
			player2.setFirstCard(bag.getTile());
			
			player1.addTile(bag.getTile());
			player1.addTile(bag.getTile());
			player1.addTile(bag.getTile());
			
			player2.addTile(bag.getTile());
			player2.addTile(bag.getTile());
			player2.addTile(bag.getTile());
			
			sequence_list.add(player1);
			sequence_list.add(player2);
			Collections.sort(sequence_list, new Comparator<HumanPlayer>() {
				@Override
				public int compare(HumanPlayer o1, HumanPlayer o2) {
					// TODO Auto-generated method stub
					return new Integer(o2.getFirstCardValue()).compareTo(new Integer(o1.getFirstCardValue()));
				}
			});
			
		}
		else if(player3!=null&&player4==null) {
			amount = 3;
			
			player1.setFirstCard(bag.getTile());
			player2.setFirstCard(bag.getTile());
			player3.setFirstCard(bag.getTile());
			
			player1.addTile(bag.getTile());
			player1.addTile(bag.getTile());
			player1.addTile(bag.getTile());
			
			player2.addTile(bag.getTile());
			player2.addTile(bag.getTile());
			player2.addTile(bag.getTile());
			
			player3.addTile(bag.getTile());
			player3.addTile(bag.getTile());
			player3.addTile(bag.getTile());
			
			sequence_list.add(player1);
			sequence_list.add(player2);
			sequence_list.add(player3);
			Collections.sort(sequence_list, new Comparator<HumanPlayer>() {
				@Override
				public int compare(HumanPlayer o1, HumanPlayer o2) {
					// TODO Auto-generated method stub
					return new Integer(o2.getFirstCardValue()).compareTo(new Integer(o1.getFirstCardValue()));
				}
			});
			
		}
		else if(player3!=null&&player4!=null) {
			amount = 4;
			
			player1.setFirstCard(bag.getTile());
			player2.setFirstCard(bag.getTile());
			player3.setFirstCard(bag.getTile());
			player4.setFirstCard(bag.getTile());
			
			player1.addTile(bag.getTile());
			player1.addTile(bag.getTile());
			player1.addTile(bag.getTile());
			
			player2.addTile(bag.getTile());
			player2.addTile(bag.getTile());
			player2.addTile(bag.getTile());
			
			player3.addTile(bag.getTile());
			player3.addTile(bag.getTile());
			player3.addTile(bag.getTile());
			
			player4.addTile(bag.getTile());
			player4.addTile(bag.getTile());
			player4.addTile(bag.getTile());
			
			sequence_list.add(player1);
			sequence_list.add(player2);
			sequence_list.add(player3);
			sequence_list.add(player4);
			Collections.sort(sequence_list, new Comparator<HumanPlayer>() {
				@Override
				public int compare(HumanPlayer o1, HumanPlayer o2) {
					// TODO Auto-generated method stub
					return new Integer(o2.getFirstCardValue()).compareTo(new Integer(o1.getFirstCardValue()));
				}
			});
			
		}
		
		server.order(sequence_list.stream().map(HumanPlayer::getUsername).collect(Collectors.toList()));
		for(HumanPlayer player:sequence_list)
			server.give(
					sequence_list.stream().map(HumanPlayer::getUsername).collect(Collectors.toList()), 
					player.getUsername(), 
					player.geTiles());
		
		while(!endGameJudge(amount)) {
			for(int i=0;i<sequence_list.size();i++) {
				server.turn(
						sequence_list.stream().map(HumanPlayer::getUsername).collect(Collectors.toList()), 
						sequence_list.get(i).getUsername());
				while(logo);
				MoveData moveData = server.getMoveData(
						sequence_list.stream().map(HumanPlayer::getUsername).collect(Collectors.toList()), 
						sequence_list.get(i).getUsername());
				if(moveData.getIndex()==0) {
					if(moveData.getTileEncoding()!=null) {
						bag.addTile(new Tile(moveData.getTileEncoding()));
						sequence_list.get(i).removeTile(new Tile(moveData.getTileEncoding()));
						Tile tile = bag.getTile();
						sequence_list.get(i).addTile(tile);
						server.give(
								sequence_list.stream().map(HumanPlayer::getUsername).collect(Collectors.toList()),
								sequence_list.get(i).getUsername(), 
								new Tile[] {tile});
					}
				}else {
					int r = (int) (Math.round((Math.sqrt(moveData.getIndex()+0.25)-0.5)));
					int c = moveData.getIndex() - r*r - r;
					int[] place = {r,c};
					sequence_list.get(i).setScore( sequence_list.get(i).getScore() +
							board.addTile(new Tile(moveData.getTileEncoding()), place, Angle.turn_0));
					sequence_list.get(i).removeTile(new Tile(moveData.getTileEncoding()));
					Tile tile = bag.getTile();
					sequence_list.get(i).addTile(tile);
					server.give(
							sequence_list.stream().map(HumanPlayer::getUsername).collect(Collectors.toList()),
							sequence_list.get(i).getUsername(), 
							new Tile[] {tile});
				}
				logo = true;
			}
		}
		
		Map<String, Integer> userList = new HashMap<>();
		userList.put(player1.getUsername(), player1.getScore());
		userList.put(player2.getUsername(), player2.getScore());
		if(player3!=null)
			userList.put(player3.getUsername(), player3.getScore());
		if(player4!=null)
			userList.put(player4.getUsername(), player4.getScore());
		server.end(userList);
	}

	public void conversion() {
		this.logo = false;
	}
	
}
