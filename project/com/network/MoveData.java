package com.network;

public class MoveData {
	private int index = -1;
	private String tileEncoding = null;
	public MoveData(int index, String tileEncoding) {
		super();
		this.index = index;
		this.tileEncoding = tileEncoding;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getTileEncoding() {
		return tileEncoding;
	}
	public void setTileEncoding(String tileEncoding) {
		this.tileEncoding = tileEncoding;
	}
	
}
