package com.network;

public class Match {
	public static final String hello = "Hello<\\w+><(?:true|false)><(?:true|false)><(?:true|false)><(?:true|false)>";
	public static final String play = "Play<(?:2|3|4)>";
	public static final String move = 
			"Move<([1-9]|[1-2]\\d|[3][0-6])><[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]>";
	public static final String skip = "(Skip|Skip\\[[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]\\])";
	public static final String waiting = "Waiting<(?:0|1|2|3)>";
	public static final String order = 
			"(Order<\\w+><\\w+>|Order<\\w+><\\w+>\\[\\w+\\]|Order<\\w+><\\w+>\\[\\w+\\]\\[\\w+\\])";
	public static final String turn = "Turn<\\w+>";
	public static final String move_cilent = 
			"Move<\\w+><([1-9]|[1-2]\\d|[3][0-6])><[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]>";
	public static final String error = "Error<\\[\\w\\]+>";
	public static final String give = 
			"(Give<\\w+><[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]>|"
			+ "Give<\\w+><[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]>\\[[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]\\]\\[[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]\\]\\[[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]\\])";
	public static final String give1 = 
			"Give<\\w+><[0-5](?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)(?:R|G|B|P|Y|W)[1-6]>";
	public static final String end = 
			"(End<<\\w+>,<[1-9][0-9]{0,}>><<\\w+>,<[1-9][0-9]{0,}>>|"
			+ "End<<\\w+>,<[1-9][0-9]{0,}>><<\\w+>,<[1-9][0-9]{0,}>>\\[<\\w+>,<[1-9][0-9]{0,}>\\]|"
			+ "End<<\\w+>,<[1-9][0-9]{0,}>><<\\w+>,<[1-9][0-9]{0,}>>\\[<\\w+>,<[1-9][0-9]{0,}>\\]\\[<\\w+>,<[1-9][0-9]{0,}>\\])";
	public static final String end2 = 
			"End<<\\w+>,<[1-9][0-9]{0,}>><<\\w+>,<[1-9][0-9]{0,}>>";
	public static final String end3 = 
			"End<<\\w+>,<[1-9][0-9]{0,}>><<\\w+>,<[1-9][0-9]{0,}>>\\[<\\w+>,<[1-9][0-9]{0,}>\\]";
	public static final String end4 = 
			"End<<\\w+>,<[1-9][0-9]{0,}>><<\\w+>,<[1-9][0-9]{0,}>>\\[<\\w+>,<[1-9][0-9]{0,}>\\]\\[<\\w+>,<[1-9][0-9]{0,}>\\]";
}
