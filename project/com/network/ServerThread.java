package com.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerThread extends Thread{
	
	private Socket socket = null;
	private InputStream inputStream = null;
	private OutputStream outputStream = null;
	private String hostname = null;
	private String username = null;
	private BufferedReader bufferedReader = null;
	private BufferedWriter bufferWriter = null;
	private Transmit transmit = null;
	
	public ServerThread(Transmit transmit,Socket socket) {
		// TODO Auto-generated constructor stub
		try {
			this.transmit = transmit;
			this.socket = socket;
			this.hostname = socket.getInetAddress().getHostAddress();
			this.inputStream = socket.getInputStream();
			this.outputStream = socket.getOutputStream();
			this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			this.bufferWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ServerThread["+ hostname +"]error:"+e.getMessage());
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			while(isConnection()) {
				String data = null;
				if((data = bufferedReader.readLine()) != null) {
					if(data.equals("TestConnection"));
					else {
						System.out.println("ServerThread["+ hostname +"]receive:"+data);
						judge(data);
					}
						
				}
			}
			transmit.close(hostname);
		} catch (IOException e) {
			// TODO: handle exception
			if(e.getMessage().indexOf("Connection reset")!=-1)
				transmit.close(hostname);
			else
				System.out.println("ServerThread["+ hostname +"]error:"+e.getMessage());
		}
	}


	private boolean isConnection() {
		
		try {
			bufferWriter.write("TestConnection\n");
			bufferWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getMessage().indexOf("Connection reset")!=-1)
				return false;
		}
		return true;
	}
	
	private void judge(String data) throws IOException {
		if(Pattern.matches(Match.hello, data)) {
			Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
			Matcher matcher = intercept.matcher(data);
			boolean chat = false;
			boolean challenge = false;
			boolean leaderboard = false;
			boolean security = false;
			for(int i=0;matcher.find();i++) {
				switch (i) {
				case 0:
					username = matcher.group();
					break;
				case 1:
					chat = Boolean.parseBoolean(matcher.group());
					break;
				case 2:
					challenge = Boolean.parseBoolean(matcher.group());
					break;
				case 3:
					leaderboard = Boolean.parseBoolean(matcher.group());
					break;
				case 4:
					security = Boolean.parseBoolean(matcher.group());
					break;
				default:
					break;
				}
			}
			bufferWriter.write(String.format("Hello<%b><%b><%b><%b>\n", chat,challenge,leaderboard,security));
			bufferWriter.flush();
			transmit.extensionSign(username, hostname, chat, challenge, leaderboard, security);
		}
		else if (Pattern.matches(Match.play, data)) {
			Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
			Matcher matcher = intercept.matcher(data);
			int amount = 0;
			if(matcher.find())
				amount = Integer.parseInt(matcher.group());
			if(username!=null)
				transmit.play(username, amount);
			else {
				bufferWriter.write("Error<YouHaveNotSetUsernameYet>\n");
				bufferWriter.flush();
			}
			
		}
		else if (Pattern.matches(Match.move, data)) {
			Pattern intercept = Pattern.compile("(?<=<).*?(?=>)");
			Matcher matcher = intercept.matcher(data);
			int index = -1;
			String tileEncoding = null;
			for(int i=0;matcher.find();i++) {
				switch (i) {
				case 0:
					index = Integer.parseInt(matcher.group());
					break;
				case 1:
					tileEncoding = matcher.group();
					break;
				default:
					break;
				}
			}
			transmit.move(username, index, tileEncoding);
		}
		else if (Pattern.matches(Match.skip, data)) {
			Pattern intercept = Pattern.compile("(?<=\\[).*?(?=\\])");
			Matcher matcher = intercept.matcher(data);
			String tileEncoding = null;
			if(matcher.find())
				tileEncoding = matcher.group();
			transmit.move(username, 0, tileEncoding);
		}
		else {
			bufferWriter.write("Error<InstructionsAreNotComprehensiblePleaseCheckTheInstructions>\n");
			bufferWriter.flush();
		}
	}
	
	public String getHostname() {
		return hostname;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void write(String data) {
		try {
			bufferWriter.write(data+"\n");
			bufferWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getMessage().indexOf("Connection reset")!=-1)
				transmit.close(hostname);
			else
				System.out.println("ServerThread["+ hostname +"]error:"+e.getMessage());
		}		
	}
	
	public boolean close() {
		try {
			if(socket.isInputShutdown()&&socket.isOutputShutdown()&&socket.isClosed())
				return true;
			else {

				inputStream.close();
				outputStream.close();
				//socket.shutdownInput();
				//socket.shutdownOutput();
				socket.close();
				return socket.isInputShutdown()&&socket.isOutputShutdown()&&socket.isClosed();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ServerThread["+ hostname +"]error:"+e.getMessage());
			return socket.isInputShutdown()&&socket.isOutputShutdown()&&socket.isClosed();
		}
	}
	
}
