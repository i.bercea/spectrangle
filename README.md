# Spectrangle

Spectrangle is an implementation of the board game with the same name. This is the first version of the game.

## Installation

Unzip the archive.
Import Spectrangle into your workplace.

## Usage

Run the class Game from the package com.project. You have two choices: a single game (played locally between a human player, the user, and the computer) and an online game, played on a server between 2 to 4 players.

### Single Game
After selecting the single game, you are asked to choose the difficulty of the computer player you will play against. You have to options: random or smart. After the difficulty has been chosen, the game can actually start. Both players are given 4 tiles and the one with the highest first tile has the first move. When making a move, you have to choose: a tile from the four you currently have in your hand, the row and column on which you want to place the tile and the permutation of the tile. If a move is not possible, you will be properly informed that you can't make that move.

###Online Game
After selecting the online game, you are asked to enter your nickname. Then, you are asked the features you want your game to have: 	chat, challenge, leaderboard, security. Now, enter the number of people you want the play with (from 1 to 3). From here on, the game develops as a single one, the only difference being that your opponent(s) are also human player(s).

## Running the tests

The JUnit Tests can be found in the package called com.test. Run them in your workplace as JUnit Test Classes. If in the JUnit Menu all runs passed, that class is working properly. The JUnit classes test the methods that have been implemented by those classes. Tests were needed when debugging the system, in order to make sure which parts are working and which are not.

## Built with

Eclipse IDE version 2018-09 (4.9.0)

## Authors

Andrei Bercea - s2170906
Yuyuan Wu - s2068753
